<?php

use Laravel\Socialite\Facades\Socialite;
    Route::group(['prefix' => '','namespace'=>'App\\Http\\Controllers\\','as' =>'front.','middleware'=>['web']], function () {
        Route::view('/','homepage');
        Route::get('/business/register','BusinessController@register')->name('business.register');
        Route::post('/business/register','BusinessController@Saveregister')->name('business.register.save');
        Route::post('/business/register/check-username', 'BusinessController@postCheckUsername')->name('business.postCheckUsername');
        Route::post('/business/register/check-email', 'BusinessController@postCheckEmail')->name('business.postCheckEmail');
    });

?>
