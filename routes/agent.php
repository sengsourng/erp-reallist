<?php
Route::group(['prefix' => env('AGENT_ROUTE'),'namespace'=>'App\\Http\\Controllers\\Agent','as' =>'agent.','middleware'=>['web','auth']], function () {
    Route::get('/', function(){
        return redirect()->route('agent.dashboard.index');
    });
    Route::get('/dashboard', 'AgentDashboardController@index')->name('dashboard.index');
    Route::get('/myads','AgentDashboardController@myads')->name('myads');
    Route::get('/myfavorite','AgentDashboardController@myfavorite')->name('myfavorite');

});
