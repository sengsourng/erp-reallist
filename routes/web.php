<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Front\PropertyController;

Route::get('lang/{locale}', function ($locale) {
    App::setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
});

Auth::routes();

Route::get('oauth/{provider}', 'App\Http\Controllers\Auth\LoginController@redirectToProvider');
Route::get('oauth/{provider}/callback','App\Http\Controllers\Auth\LoginController@handleProviderCallback');

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/properties', [PropertyController::class, 'index'])->name('front.properties');





