<?php

    Route::group(['prefix' => env('ADMIN_ROUTE'),'namespace'=>'App\\Http\\Controllers\\Admin','as' =>'admin.','middleware'=>['web','auth']], function () {
        Route::get('/', function(){
            // return redirect()->route('admin.users.index');
            return redirect()->route('admin.dashboard.index');
        });
        // Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

        // Route::get('/dashboard','DashboardController')->name('admin.dashboard');
        Route::resource('/dashboard','DashboardController');
        Route::resource('/users','UserController');
        Route::resource('/roles','RoleController');
        Route::resource('/permissions','PermissionController');
    });
?>
