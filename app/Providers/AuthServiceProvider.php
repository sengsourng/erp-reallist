<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Implicitly grant "Super Admin" role all permission checks using can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('Super-Admin') ? true : null;
        });

        // Gate::before(function ($user, $ability) {
        //     if (in_array($ability, ['backup', 'superadmin',
        //         'manage_modules'])) {
        //         $administrator_list = config('constants.administrator_usernames');
        //         if (in_array($user->username, explode(',', $administrator_list))) {
        //             return true;
        //         }
        //     } else {
        //         if ($user->hasRole('Admin#' . $user->business_id)) {
        //             return true;
        //         }
        //     }
        // });
    }
}
