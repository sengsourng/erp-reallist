<?php

namespace App\Traits;

use App\Models\User;
use App\Models\Business;
use App\Models\Currency;
use App\Models\Location;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class BusinessUtil extends Util{
    /**
     * Gives a list of all currencies
     *
     * @return array
     */
    public function allCurrencies()
    {
        $currencies = Currency::select('id', DB::raw("concat(country, ' - ',currency, '(', code, ') ') as info"))
                ->orderBy('country')
                ->pluck('info', 'id');
        return $currencies;
    }

    /**
     * Gives a list of all timezone
     *
     * @return array
     */
    public function allTimeZones()
    {
        $datetime = new \DateTimeZone("EDT");
        $timezones = $datetime->listIdentifiers();
        $timezone_list = [];
        foreach ($timezones as $timezone) {
            $timezone_list[$timezone] = $timezone;
        }
        return $timezone_list;
    }

    /**
     * Gives a list of all accouting methods
     *
     * @return array
     */
    public function allAccountingMethods()
    {
        return [
            'fifo' => __('cruds.bussiness.fifo'),
            'lifo' => __('cruds.bussiness.lifo')
        ];
    }


    /**
     * Creates new business with default settings.
     *
     * @return array
     */
    public function createNewBusiness($business_details)
    {
        $business_details['sell_price_tax'] = 'includes';
        $business_details['default_profit_percent'] = 25;

        //Add POS shortcuts
        $business_details['keyboard_shortcuts'] = '{"pos":{"express_checkout":"shift+e","pay_n_ckeckout":"shift+p","draft":"shift+d","cancel":"shift+c","edit_discount":"shift+i","edit_order_tax":"shift+t","add_payment_row":"shift+r","finalize_payment":"shift+f","recent_product_quantity":"f2","add_new_product":"f4"}}';

        //Add prefixes
        $business_details['ref_no_prefixes'] = [
            'purchase' => 'PO',
            'stock_transfer' => 'ST',
            'stock_adjustment' => 'SA',
            'sell_return' => 'CN',
            'expense' => 'EP',
            'contacts' => 'CO',
            'purchase_payment' => 'PP',
            'sell_payment' => 'SP',
            'business_location' => 'BL'
            ];

        //Disable inline tax editing
        $business_details['enable_inline_tax'] = 0;

        $business = Business::create_business($business_details);

        return $business;
    }

    /**
     * Adds a new location to a business
     *
     * @param int $business_id
     * @param array $location_details
     * @param int $invoice_layout_id default null
     *
     * @return location object
     */
    public function addLocation($business_id, $location_details, $invoice_scheme_id = null, $invoice_layout_id = null)
    {
        // if (empty($invoice_scheme_id)) {
        //     $layout = InvoiceLayout::where('is_default', 1)
        //                             ->where('business_id', $business_id)
        //                             ->first();
        //     $invoice_layout_id = $layout->id;
        // }

        // if (empty($invoice_scheme_id)) {
        //     $scheme = InvoiceScheme::where('is_default', 1)
        //                             ->where('business_id', $business_id)
        //                             ->first();
        //     $invoice_scheme_id = $scheme->id;
        // }

        // //Update reference count
        $ref_count = $this->setAndGetReferenceCount('business_location', $business_id);
        $location_id = $this->generateReferenceNumber('business_location', $ref_count, $business_id);

        // //Enable all payment methods by default
        // $payment_types = $this->payment_types();
        // $location_payment_types = [];
        // foreach ($payment_types as $key => $value) {
        //     $location_payment_types[$key] = [
        //         'is_enabled' => 1,
        //         'account' => null
        //     ];
        // }
        $location = Location::create(['business_id' => $business_id,
                            'name' => $location_details['name'],
                            'location_id' => $location_id,
                            // 'landmark' => $location_details['landmark'],
                            // 'state' => $location_details['state'],
                            'city' => $location_details['city'],
                            'zip_code' => $location_details['zip_code'],
                            'country' => $location_details['country'],
                            // 'invoice_scheme_id' => $invoice_scheme_id,
                            // 'invoice_layout_id' => $invoice_layout_id,
                            // 'sale_invoice_layout_id' => $invoice_layout_id,
                            // 'mobile' => !empty($location_details['mobile']) ? $location_details['mobile'] : '',
                            // 'alternate_number' => !empty($location_details['alternate_number']) ? $location_details['alternate_number'] : '',
                            'website' => !empty($location_details['website']) ? $location_details['website'] : '',
                            // 'default_payment_accounts' => json_encode($location_payment_types)
                        ]);
        return $location;
    }

    // /**
        //  * Adds a default settings/resources for a new business
        //  *
        //  * @param int $business_id
        //  * @param int $user_id
        //  *
        //  * @return boolean
        //  */
    public function newBusinessDefaultResources($business_id, $user_id)
    {
        $user = User::find($user_id);

        //create Admin role and assign to user
        $role = Role::create([ 'name' => 'Admin#' . $business_id,
                            'business_id' => $business_id,
                            'guard_name' => 'web', 'is_default' => 1
                        ]);
        $user->assignRole($role->name);

        //Create Cashier role for a new business
        $cashier_role = Role::create([ 'name' => 'Cashier#' . $business_id,
                            'business_id' => $business_id,
                            'guard_name' => 'web'
                        ]);
        $cashier_role->syncPermissions(['sell.view', 'sell.create', 'sell.update', 'sell.delete', 'access_all_locations', 'view_cash_register', 'close_cash_register']);

        // $business = Business::findOrFail($business_id);

        //Update reference count
        // $ref_count = $this->setAndGetReferenceCount('contacts', $business_id);
        // $contact_id = $this->generateReferenceNumber('contacts', $ref_count, $business_id);

        // //Add Default/Walk-In Customer for new business
        // $customer = [
        //                 'business_id' => $business_id,
        //                 'type' => 'customer',
        //                 'name' => 'Walk-In Customer',
        //                 'created_by' => $user_id,
        //                 'is_default' => 1,
        //                 'contact_id' => $contact_id,
        //                 'credit_limit' => 0
        //             ];
        // Contact::create($customer);

        // //create default invoice setting for new business
        // InvoiceScheme::create(['name' => 'Default',
        //                     'scheme_type' => 'blank',
        //                     'prefix' => '',
        //                     'start_number' => 1,
        //                     'total_digits' => 4,
        //                     'is_default' => 1,
        //                     'business_id' => $business_id
        //                 ]);
        // //create default invoice layour for new business
        // InvoiceLayout::create(['name' => 'Default',
        //                 'header_text' => null,
        //                 'invoice_no_prefix' => 'Invoice No.',
        //                 'invoice_heading' => 'Invoice',
        //                 'sub_total_label' => 'Subtotal',
        //                 'discount_label' => 'Discount',
        //                 'tax_label' => 'Tax',
        //                 'total_label' => 'Total',
        //                 'show_landmark' => 1,
        //                 'show_city' => 1,
        //                 'show_state' => 1,
        //                 'show_zip_code' => 1,
        //                 'show_country' => 1,
        //                 'highlight_color' => '#000000',
        //                 'footer_text' => '',
        //                 'is_default' => 1,
        //                 'business_id' => $business_id,
        //                 'invoice_heading_not_paid' => '',
        //                 'invoice_heading_paid' => '',
        //                 'total_due_label' => 'Total Due',
        //                 'paid_label' => 'Total Paid',
        //                 'show_payments' => 1,
        //                 'show_customer' => 1,
        //                 'customer_label' => 'Customer',
        //                 'table_product_label' => 'Product',
        //                 'table_qty_label' => 'Quantity',
        //                 'table_unit_price_label' => 'Unit Price',
        //                 'table_subtotal_label' => 'Subtotal',
        //                 'date_label' => 'Date'
        //             ]);

        // //Add Default Unit for new business
        // $unit = [
        //             'business_id' => $business_id,
        //             'actual_name' => 'Pieces',
        //             'short_name' => 'Pc(s)',
        //             'allow_decimal' => 0,
        //             'created_by' => $user_id
        //         ];
        // Unit::create($unit);

        // //Create default notification templates
        // $notification_templates = NotificationTemplate::defaultNotificationTemplates($business_id);
        // foreach ($notification_templates as $notification_template) {
        //     NotificationTemplate::create($notification_template);
        // }

        return true;
    }
}
