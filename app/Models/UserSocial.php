<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','name', 'provider_name', 'provider_id','icon','value','color'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
