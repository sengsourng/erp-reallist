<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'start_date',
        'time_zone',
        'fy_start_month',
        'accounting_method',
        'logo',
        'currency_id',
        'package_id',
        'owner_id',
        'category_id',
        'tax_number_1',
        'tax_label_1',
        'tax_number_2',
        'tax_label_2'
    ];

    protected $casts = [
        'ref_no_prefixes' => 'array',
        'enabled_modules' => 'array',
        'email_settings' => 'array',
        'sms_settings' => 'array',
        'common_settings' => 'array',
        'weighing_scale_setting' => 'array'
    ];

    /**
     * Returns the date formats
     */
    public static function date_formats()
    {
        return [
            'd-m-Y' => 'dd-mm-yyyy',
            'm-d-Y' => 'mm-dd-yyyy',
            'd/m/Y' => 'dd/mm/yyyy',
            'm/d/Y' => 'mm/dd/yyyy'
        ];
    }

    public static function create_business($details)
    {
        $business = Business::create($details);
        return $business;
    }
}
