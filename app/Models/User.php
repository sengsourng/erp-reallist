<?php

namespace App\Models;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, SoftDeletes;

    protected $with = ['roles','permissions'];

    protected $fillable = [
        'usertype',
        'position',
        'surname',
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'usertype',
        'role_id',
        'position',
        'phone',
        'alt_phone',
        'language',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function create_user($details)
    {
        $user = User::create([
                    'surname' => $details['surname'],
                    'first_name' => $details['first_name'],
                    'last_name' => $details['last_name'],
                    'username' => $details['username'],
                    'email' => $details['email'],
                    'password' =>bcrypt($details['password']),
                    'phone' => $details['phone'],
                    'alt_phone' => $details['alt_phone'],
                    'language' => !empty($details['language']) ? $details['language'] : 'en',
                ]);
        return $user;
    }

    public function identities() {
        return $this->hasMany(UserSocial::class);
    }
}
