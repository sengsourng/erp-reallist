<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EcomApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('API-TOKEN');
        // $is_api_settings_exists = EcomApiSetting::where('api_token', $token)
        //                                         ->exists();
        // if (!$is_api_settings_exists) {
        //     die('Invalid Request');
        // }
        return $next($request);
    }
}
