<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\System;
use App\Traits\ModuleUtil;
use App\Traits\BusinessUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\directoryExists;

class BusinessController extends Controller
{
    protected $businessUtil;
    protected $moduleUtil;
    protected $file_upload_path;
    protected $image_upload_path;
    protected $file_path = 'business_document';
    protected $image_path ='business_logos';

    public function __construct(BusinessUtil $businessUtil, ModuleUtil $moduleUtil)
    {
        $this->businessUtil = $businessUtil;
        $this->moduleUtil = $moduleUtil;
        $this->file_upload_path = public_path().DIRECTORY_SEPARATOR.'uploads/documents'.DIRECTORY_SEPARATOR.$this->file_path.DIRECTORY_SEPARATOR;
        $this->image_upload_path = public_path().DIRECTORY_SEPARATOR.'uploads/images'.DIRECTORY_SEPARATOR.$this->image_path.DIRECTORY_SEPARATOR;
    }
    public function register()
    {
        $data['roles'] = Role::pluck('name','id');
        $data['currencies'] = $this->businessUtil->allCurrencies();
        $data['timezone_list'] = $this->businessUtil->allTimeZones();
        $data['months'] = [];
        for ($i=1; $i<=12; $i++) {
            $data['months'][$i] = __('cruds.bussiness.months.' . $i);
        }
        $data['accounting_methods'] = $this->businessUtil->allAccountingMethods();
        $data['package_id'] = request()->package;
        $data['system_settings'] = System::getProperties(['superadmin_enable_register_tc', 'superadmin_register_tc'], true);
        // return $data;
        return view('bussiness.register',$data);
    }

    public function Saveregister(Request $request)
    {
        try {
            if(!file_exists($this->image_upload_path)){
                mkdir($this->image_upload_path,0777,true);
            }
            $validator = Validator::make($request->all(), [
                'surname' => 'max:10',
                'first_name' => 'required|max:255',
                'email' => 'sometimes|nullable|email|unique:users|max:255',
                'username' => 'required|min:4|max:255|unique:users',
                'password' => 'required|confirmed|min:6|max:255',
                'phone' => 'required|min:9|max:15',
                'name' => 'required|max:255',
                'currency_id' => 'required|numeric',
                'country' => 'required|max:255',
                'city' => 'required|max:255',
                'zip_code' => 'required|max:255',
                'time_zone' => 'required|max:255',
                'fy_start_month' => 'required',
                'accounting_method' => 'required',
            ]);

            if ($validator->fails()) {
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }

            DB::beginTransaction();

            //Create owner.
            $user_details = $request->only(['surname', 'first_name', 'last_name', 'username', 'email', 'password', 'language','phone','alt_phone']);
            $user_details['language'] = empty($user_details['language']) ? config('app.locale') : $user_details['language'];
            $user = User::create_user($user_details);
            $business_details = $request->only(['name', 'start_date', 'currency_id', 'time_zone','tax_number_1','tax_label_1','tax_number_2','tax_label_2']);
            $business_details['fy_start_month'] = 1;
            $business_location = $request->only(['name', 'country', 'city', 'zip_code', 'website']);
            //Create the business
            $business_details['owner_id'] = $user->id;
            if (!empty($business_details['start_date'])) {
                $business_details['start_date'] = Carbon::createFromFormat(config('constants.default_date_format'), $business_details['start_date'])->toDateString();
            }

            //upload logo
            $logo_name = $this->businessUtil->uploadFile($request, 'business_logo', $this->image_upload_path, 'image');
            if (!empty($logo_name)) {
                $business_details['logo'] = $logo_name;
            }

            //default enabled modules
            $business_details['enabled_modules'] = ['purchases','add_sale','pos_sale','stock_transfers','stock_adjustment','expenses'];
            $business = $this->businessUtil->createNewBusiness($business_details);
            //Update user with b, usiness id
            $user->business_id = $business->id;
            $user->save();

            // $this->businessUtil->newBusinessDefaultResources($business->id, $user->id);
            $new_location = $this->businessUtil->addLocation($business->id, $business_location);

            //create new permission with the new location
            // Permission::create(['name' => 'location.' . $new_location->id ]);

            DB::commit();
            return redirect('/')->with('toast_success', __('cruds.business.business_created_succesfully'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            $output = ['success' => 0,
                            'msg' => __('messages.something_went_wrong')
                        ];
            return back()->with('status', $output)->withInput();
        }

    }

    public function postCheckUsername(Request $request)
    {
        $username = $request->input('username');
        if (!empty($request->input('username_ext'))) {
            $username .= $request->input('username_ext');
        }
        $count = User::where('username', $username)->count();
        if ($count == 0) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }

    public function postCheckEmail(Request $request)
    {
        $email = $request->input('email');

        $query = User::where('email', $email);

        if (!empty($request->input('user_id'))) {
            $user_id = $request->input('user_id');
            $query->where('id', '!=', $user_id);
        }
        $exists = $query->exists();
        if (!$exists) {
            echo "true";
            exit;
        } else {
            echo "false";
            exit;
        }
    }
}
