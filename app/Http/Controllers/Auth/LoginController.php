<?php

namespace App\Http\Controllers\Auth;

use Exception;
use App\Models\User;
use App\Models\UserSocial;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
        |--------------------------------------------------------------------------
        | Login Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles authenticating users for the application and
        | redirecting them to your home screen. The controller uses a trait
        | to conveniently provide its functionality to your applications.
        |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
      if(is_numeric($request->get('email'))){
        return ['phone'=>$request->get('email'),'password'=>$request->get('password')];
      }
      elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
        return ['email' => $request->get('email'), 'password'=>$request->get('password')];
      }
      return ['username' => $request->get('email'), 'password'=>$request->get('password')];
    }

    protected function authenticated($request, $user)
    {
        if ($user->hasRole('Admin')) {
            $this->redirectTo = route(config('author.admin_route').'.dashboard.index');
        } elseif ($user->hasRole('Owner')) {
            $this->redirectTo = route(config('author.owner_route').'.dashboard.index');
        } elseif ($user->hasRole('Agent')) {
            $this->redirectTo = route(config('author.agent_route').'.dashboard.index');
        } elseif ($user->hasRole('Staff')) {
            $this->redirectTo = route(config('author.staff_route').'.dashboard.index');
        } elseif ($user->hasRole('User')) {
            $this->redirectTo = route(config('author.user_route').'.dashboard.index');
        }
    }

    public function redirectToProvider($provider)
    {
      return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->stateless()->user();
        } catch (Exception $e) {
            dd($e->getMessage());
            return redirect('/login');
        }

        $authUser = $this->findOrCreateUser($user, $provider);
        $authUser->assignRole(5);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($providerUser, $provider)
    {
        $account = UserSocial::whereProviderName($provider)
                            ->whereProviderId($providerUser->getId())
                            ->first();
        if ($account) {
            return $account->user;
        } else {
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (! $user) {
                $fullnames  = $providerUser->getName();
                $names = explode(" ", $fullnames);
                $fname = $names[0]??null; // piece1
                $lname= $names[1]??null; // piece2
                $usernames = $providerUser->getEmail();
                $unames = explode("@", $usernames);
                $uname = $unames[0]??null;
                    $user = User::create([
                        'email' => $providerUser->getEmail(),
                        'first_name'  => $fname,
                        'last_name'  =>  $lname,
                        'username'  => $uname,
                        'password' => bcrypt('123456')
                    ]);
            }
            $user->identities()->create([
                'provider_id'   => $providerUser->getId(),
                'provider_name' => $provider,
            ]);
            return $user;
        }
    }

}
