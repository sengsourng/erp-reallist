<?php

namespace App\Http\Controllers\Admin;


use DB;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $user = User::where('id',Auth::id())->first();
        $roles = $user->getRoleNames();
        foreach($roles as $role){
            if($role==='Super-Admin'){
                $data['roles'] = Role::orderBy('id','asc')->paginate(5);
            } else {
                $data['roles'] = Role::where('slug','<>','super-admin')->orderBy('id','asc')->paginate(5);
            }
        }
        return view('admin.roles.index',$data)
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
		$permissions = Permission::all(); //Get all permissions
		$data['permissions'] = $permissions->groupBy('group');
        return view('admin.roles.create',$data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'slug' => 'required|unique:roles,slug',
            'permission' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        $role = Role::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description')
        ]);
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('admin.roles.index')
                        ->with('toast_success','Role created successfully');
    }

    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();

        return view('admin.roles.show',compact('role','rolePermissions'));
    }

    public function edit($id)
    {
		$data['role'] = Role::findOrFail($id);
		$permissions = Permission::all();
		$data['permissions'] = $permissions->groupBy('group');
		$data['role_permission'] = $data['role']->permissions()->pluck('id','id')->toArray();
        return view('admin.roles.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => "required|unique:roles,name,$id",
            'slug' => "required|unique:roles,slug,$id",
            'permission' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->slug = $request->input('slug');
        $role->description = $request->input('description');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('admin.roles.index')
                        ->with('toast_success','Role updated successfully');
    }

    public function destroy($id)
    {
        Role::where('id',$id)->delete();
        // DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('admin.roles.index');
    }
}
