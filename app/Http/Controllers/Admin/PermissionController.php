<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    public function index()
    {
        $data['permissions'] = Permission::all();
        return view('admin.permissions.index',$data);
    }

    public function create()
    {
        return view('admin.permissions.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group' => 'required',
            'name' => 'required|unique:roles,name',
            'slug' => 'required|unique:roles,slug',
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        $permission = Permission::create($request->all());
        if($permission->save()){
            return redirect()->route('admin.permissions.index')
            ->with('toast_success','Permission is created successfully');
        }
    }

    public function show($id)
    {
        $data['permission'] = Permission::FindOrFail($id);
        return view('admin.permissions.show',$data);
    }

    public function edit($id)
    {
        $data['permission'] = Permission::FindOrFail($id);
        return view('admin.permissions.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'group' => 'required',
            'name' => "required|unique:roles,name,$id",
            'slug' => "required|unique:roles,slug,$id"
        ]);
        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        $permission = Permission::FindOrFail($id)->update($request->all());
        if($permission){
            return redirect()->route('admin.permissions.index')
            ->with('toast_success','Permission is  updated successfully');
        }
    }

    public function destroy($id)
    {
        $permission = Permission::FindOrFail($id);
        if($permission->delete()){
            return redirect()->back();
        }
    }
}
