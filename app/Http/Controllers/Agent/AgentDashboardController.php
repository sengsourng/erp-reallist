<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentDashboardController extends Controller
{
    public function index()
    {
        if(auth()->user()->hasRole('Agent')==1){
            // echo "Agent Page ";

            return view('agent.dashboard.dashboard');
        }
        // return view('home');
    }



    public function myads()
    {
        if(auth()->user()->hasRole('Agent')==1){
            // echo "Agent Page ";

            return view('agent.myads');
        }
        // return view('home');
    }


    public function myfavorite()
    {
        if(auth()->user()->hasRole('Agent')==1){
            // echo "Agent Page ";

            return view('agent.myfavorite');
        }
        // return view('home');
    }


}
