<!doctype html>
<html lang="en">

<head>
    <title>jQuery Smart Wizard - The awesome jQuery step wizard plugin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" crossorigin="anonymous">
    <!-- Include SmartWizard CSS -->
    <link href="{{ asset('public\backend\node_modules\smartwizard\dist\css\smart_wizard_all.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Demo Page Style - You don't need -->
    <style>
        body {
            padding-right: 5%;
            padding-left: 5%;
        }
    </style>

</head>

<body>
    <div class="containger">
        <form action="#" method="post" role="form" data-toggle="validator" accept-charset="utf-8"></form>
            <!-- SmartWizard html -->
            <div id="smartwizard">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#step-1">
                            <strong>Step 1</strong> <br>Name
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step-2">
                            <strong>Step 2</strong> <br>Email
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step-3">
                            <strong>Step 3</strong> <br>Phone
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#step-4">
                            <strong>Step 4</strong> <br>Address
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#step-4">
                            <strong>Step 4</strong> <br>Term and Conditions
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                        <h3>Your Name</h3>
                        <div id="form-step-0" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" id="name" placeholder="Enter Your Name" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                        <h3>Your Email Address</h3>
                        <div id="form-step-1" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" name="email" id="email" placeholder="Enter Your Email" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                        <h3>Your Phoone</h3>
                        <div id="form-step-2" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="phone">Phone:</label>
                                <input type="text" name="phone" id="phone" placeholder="Enter Your Phone" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
                        <h3>Your Address</h3>
                        <div id="form-step-3" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="address">Address:</label>
                                <input type="text" name="address" id="address" placeholder="Enter Your Address" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div id="step-5" class="tab-pane" role="tabpanel" aria-labelledby="step-5">
                        <h3>Your Term & Conditions</h3>
                        <div id="form-step-4" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="terms">I aggress with the T&C</label>
                                <input type="checkbox" name="terms" id="terms" data-error="Please accept the Term and Condition" required >
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <br />

    <!-- Include jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <!-- Include SmartWizard JavaScript source -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('public\backend\node_modules\smartwizard\dist\js\jquery.smartWizard.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // Toolbar extra buttons
            var stepCount = $('#smartwizard').find('li > a').length;
            var btnFinish = $('<button></button>').text('SUBMIT')
                                  .addClass('btn btn-info sw-btn-group-extra d-none')
                                  .on('click', function(){
                                    alert('Finish');
                                  });
            var selectedStep = 0;
            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows', // default, arrows, dots, progress
                // darkMode: true,
                transition: {
                    animation: 'fade', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                },
                toolbarSettings: {
                    toolbarPosition: 'bottom', // both bottom
                    toolbarExtraButtons: [btnFinish]
                },
            });

            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
                if (stepPosition != 'last') {
                    $(".sw-btn-next").removeClass('d-none');
                    $('.sw-btn-group-extra').addClass('d-none');
                } else if (stepPosition === 'last') {
                    $(".sw-btn-next").addClass('d-none');
                    $('.sw-btn-group-extra').removeClass('d-none')
                } else {
                    $(".sw-btn-next").removeClass('d-none');
                    $("#next-btn").removeClass('disabled');
                }
            });

            $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
                var elmForm = $("#form-step-" + currentStepIndex);
                if(stepDirection === 'forward' && elmForm){
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    if(elmErr && elmErr.length > 0){
                        return false;
                    }
                }
                return true;
            });
        });
    </script>
</body>

</html>




{{-- <div class="modal fade modal-xl" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Smart Wizard modal</h5>
                <div class="card-toolbar mr-auto no-border">
                    <label class="mb-0">
                      <span class="align-middle d-block d-sm-inline text-600">
                        Input Validation:
                      </span>
                    <input type="checkbox" id="id-validate" class="input-lg text-secondary-l1 bgc-purple-d1 ml-2 ace-switch ace-switch-onoff align-middle">
                  </label>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div id="smartwizard-1" class="d-none mx-n3 mx-sm-auto">
                    <ul class="mx-auto">
                        <li class="wizard-progressbar"></li>
                        <!-- the progress line connecting wizard steps -->
                        <li>
                            <a href="#step-1">
                            <span class="step-title">
                            1
                            </span>
                            <span class="step-title-done">
                            <i class="fa fa-check text-success"></i>
                            </span>
                            </a>
                            <span class="step-description">
                            Validation States
                            </span>
                        </li>
                        <li>
                            <a href="#step-2">
                            <span class="step-title">
                            2
                            </span>
                            <span class="step-title-done">
                            <i class="fa fa-check text-success"></i>
                            </span>
                            </a>
                            <span class="step-description">
                            Alerts
                            </span>
                        </li>
                        <li>
                            <a href="#step-3">
                            <span class="step-title">
                            3
                            </span>
                            <span class="step-title-done">
                            <i class="fa fa-check text-success"></i>
                            </span>
                            </a>
                            <span class="step-description">
                            Payment Info
                            </span>
                        </li>
                        <li>
                            <a href="#step-4">
                            <span class="step-title">
                            4
                            </span>
                            <span class="step-title-done">
                            <i class="fa fa-check text-success"></i>
                            </span>
                            </a>
                            <span class="step-description">
                            Other Info
                            </span>
                        </li>
                    </ul>
                    <div class="px-2 py-2 mb-4">
                        <div id="step-1">
                            <!-- if "Input Validation" is selected, we should validate this form before going to next step -->
                            <form id="validation-form" class="mt-4 text-dark-m1">
                                <h4 class="text-primary mb-4 ml-md-4">
                                Enter the following information
                                </h4>

                                <div class="form-group row mt-2">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                    Email Address:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                    <input required type="email" name="email" class="form-control col-11 col-sm-8 col-md-6" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                    Password:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                    <input required type="password" id="password" name="password" class="form-control col-11 col-sm-6 col-md-4" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                    Confirm Password:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                        <input required type="password" name="password2" class="form-control col-11 col-sm-6 col-md-4" placeholder="" />
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                    Company Name:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                    <input type="text" name="company" class="form-control col-11 col-sm-8 col-md-5" placeholder="Optional ..." />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                        Phone Number:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                        <input required type="text" name="phone" class="form-control col-11 col-sm-8 col-md-3" id="phone" placeholder="" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                        Company URL:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                        <input required type="url" name="url" class="form-control col-11 col-sm-8 col-md-8" id="url" placeholder="" />
                                    </div>
                                </div>
                                <hr />

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                        Subscribe to:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3 pt-1">
                                        <div>
                                            <label>
                                            <input type="checkbox" id="id-check-1" name="subscription" value="1" class="mr-1 align-sub" />
                                            Latest news and announcements
                                            </label>
                                        </div>

                                        <div>
                                            <label>
                                            <input type="checkbox" id="id-check-2" name="subscription" value="2" class="mr-1 align-sub" />
                                            Product offers and discounts
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                    Gender:
                                    </div>
                                    <div class="col-sm-9 pr-0 pr-sm-3 pt-1">
                                        <div>
                                            <label>
                                            <input type="radio" id="id-radio-2" name="gender" value="1" class="mr-1 align-sub" />
                                            Male
                                            </label>
                                        </div>

                                        <div>
                                            <label>
                                            <input type="radio" id="id-radio-3" name="gender" value="2" class="mr-1 align-sub" />
                                            Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <hr />

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                        Platform:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                        <select class="form-control col-11 col-sm-4" id="platform" name="platform" data-placeholder="Click to Choose...">
                                            <option value="">&nbsp;</option>
                                            <option value="linux">Linux</option>
                                            <option value="windows">Windows</option>
                                            <option value="mac">Mac OS</option>
                                            <option value="ios">iOS</option>
                                            <option value="android">Android</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label text-sm-right pr-0">
                                        Comment:
                                    </div>

                                    <div class="col-sm-9 pr-0 pr-sm-3">
                                        <textarea class="form-control col-11 col-sm-12 col-md-6" name="comment" id="comment"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row mt-4">
                                    <div class="col-sm-9 pr-0 pr-sm-3 pt-1 offset-sm-3">
                                        <label>
                                            <input required type="checkbox" class="border-2 brc-success-m1 brc-on-checked input-lg text-dark-tp3 mr-1" id="id-check-terms" name="agree" />
                                            I agree to the terms of use
                                        </label>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div id="step-2">
                            <div class="row">
                                <div class="col-12 col-xl-10 offset-xl-1">
                                    <div role="alert" class="alert alert-lg bgc-success-l3 border-0 border-l-4 brc-success mt-4 mb-3 pr-3 py-3 d-flex">
                                        <div class="flex-grow-1">
                                            <i class="fas fa-check mr-1 text-120 text-success-m1"></i>
                                            <strong class="text-success-d1">Well done!</strong>
                                            <span class="text-105 text-dark-tp2">You successfully read this important alert message.</span>
                                        </div>

                                        <button type="button" class="close align-self-start" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="fa fa-times text-80"></i></span>
                                        </button>
                                    </div>

                                    <div role="alert" class="alert alert-lg bgc-secondary-l4 text-dark-m3 border-0 border-l-4 brc-danger-m1 mb-3 pr-3 py-3 d-flex">
                                        <div class="flex-grow-1">
                                            <i class="fas fa-times mr-1 text-danger"></i>
                                            <strong class="text-danger-m1">Oh snap!</strong> Change a few things up and try submitting again.
                                        </div>

                                        <button type="button" class="close text-danger align-self-start" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="fa fa-times text-80"></i></span>
                                        </button>
                                    </div>

                                    <div role="alert" class="alert alert-warning alert-lg mb-3 border-0 pr-3 py-3 d-flex">
                                        <div class="flex-grow-1">
                                        <i class="fas fa-exclamation mr-2px text-orange-d2"></i>
                                        <strong class="text-orange-d3">Warning !</strong> Best check yo self, you're not looking too good.
                                        </div>

                                        <button type="button" class="close text-brown align-self-start" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="fa fa-times text-80"></i></span>
                                        </button>
                                    </div>

                                    <div role="alert" class="alert alert-lg bgc-blue-l4 border-0 border-t-3 brc-blue-m2 mb-3 radius-0 pr-3 py-3 d-flex">
                                        <div class="flex-grow-1">
                                            <strong class="text-blue-d2">Heads up!</strong> This alert needs your attention, but it's not super important.
                                        </div>

                                        <button type="button" class="close align-self-start" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="fa fa-times text-80"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="step-3" class="text-center">
                            <h3 class="font-light text-primary my-4">
                            Select Payment Method
                            </h3>
                            <form autocomplete="off" class="col-sm-9 col-lg-6 col-xl-5 mx-auto d-block btn-group btn-group-toggle" data-toggle="buttons">

                                <div role="button" class="d-block d-style border-l-3 py-3 mb-2 btn btn-outline-light btn-h-outline-purple btn-a-outline-purple btn-h-bgc-tp radius-1">
                                    <input type="radio" name="payments" id="payments1" />
                                    <div class="d-flex align-items-center">

                                        <div class="w-6 pos-rel">
                                        <div class="v-active bgc-success-tp1 radius-round py-1 position-lc">
                                        <i class="fa fa-check text-white px-2"></i>
                                        </div>
                                        <div class="v-n-active px-2 position-lc">
                                        <i class="fa fa-circle text-secondary-l3 text-90"></i>
                                        </div>
                                        </div>

                                        <div class="text-600 text-purple-d1 text-left flex-grow-1">
                                        Visa/Mastercard
                                        <br />
                                        <span class="text-dark-l1 text-400">
                                        flexible
                                        </span>
                                        </div>

                                        <div class="mr-3">
                                        <i class="fab fa-cc-mastercard text-200 text-purple-d1"></i>
                                        </div>

                                    </div>
                                </div>

                                <div role="button" class="d-block d-style border-l-3 py-3 mb-2 btn btn-outline-light btn-h-outline-primary btn-a-outline-primary btn-h-bgc-tp radius-1">
                                    <input type="radio" name="payments" id="payments2" />
                                    <div class="d-flex align-items-center">

                                        <div class="w-6 pos-rel">
                                        <div class="v-active bgc-success-tp1 radius-round py-1 position-lc">
                                        <i class="fa fa-check text-white px-2"></i>
                                        </div>
                                        <div class="v-n-active px-2 position-lc">
                                        <i class="fa fa-circle text-secondary-l3 text-90"></i>
                                        </div>
                                        </div>

                                        <div class="text-600 text-blue-d1 text-left flex-grow-1">
                                        Paypal
                                        <br />
                                        <span class="text-dark-l1 text-400">
                                        fast &amp; easy
                                        </span>
                                        </div>

                                        <div class="mr-3">
                                        <i class="fab fa-cc-paypal text-200 text-blue-m1"></i>
                                        </div>

                                    </div>
                                </div>

                                <div role="button" class="d-block d-style border-l-3 py-3 mb-2 btn btn-outline-light btn-h-outline-warning btn-a-outline-warning btn-h-bgc-tp radius-1">
                                    <input type="radio" name="payments" id="payments3" />
                                    <div class="d-flex align-items-center">
                                        <div class="w-6 pos-rel">
                                        <div class="v-active bgc-success-tp1 radius-round py-1 position-lc">
                                        <i class="fa fa-check text-white px-2"></i>
                                        </div>
                                        <div class="v-n-active px-2 position-lc">
                                        <i class="fa fa-circle text-secondary-l3 text-90"></i>
                                        </div>
                                        </div>

                                        <div class="text-600 text-orange-d2 text-left flex-grow-1">
                                        Bitcoin
                                        <br />
                                        <span class="text-dark-l1 text-400">
                                        state of the art
                                        </span>
                                        </div>
                                        <div class="mr-3">
                                        <i class="fab fa-bitcoin text-200 text-orange-d1"></i>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div id="step-4" class="text-center">
                            <h3 class="text-400 text-success mt-4"> Congrats! </h3>
                            Your product is ready to ship! Click finish to continue!
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
