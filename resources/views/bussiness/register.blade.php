<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <base href="{{asset('/')}}" />
    <title>@yield('pagetitle','Reallist Home Page')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- include common vendor stylesheets & fontawesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/regular.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/brands.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/solid.css')}}">
    <!-- include fonts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/backend') }}/node_modules/select2/dist/css/select2.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/backend') }}/node_modules/tiny-date-picker/tiny-date-picker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/backend') }}/node_modules/tiny-date-picker/date-range-picker.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/backend') }}/node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/dist/css/ace-font.css')}}">
    <!-- ace.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/dist/css/ace.css')}}">
    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{asset('public/backend/favicon.png')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/dist/css/ace-themes.css')}}">
    <link rel="stylesheet" href="{{ asset('public/front') }}/steps/css/main.css">
    <link rel="stylesheet" href="{{ asset('public/front') }}/steps/css/jquery.steps.css">
    <style>
        body{
            font-family: 'Hanuman', 'serif' !important;
        }
        .wizard > .content {
            min-height: 41rem;
        }
        .wizard, .tabcontrol {
            z-index: -1;
        }
    </style>
  </head>

<body>
    @include('frontend.layouts.top_nav')

    <div class="container-fluid bgc-white">


        <div class="row">
            <div class="col-md-4 pt-12">
                <img style="vertical-align: middle;
                display: inline-block;
                position: relative;max-width: 100%;padding-top: 100px !important;" src="{{asset('public/images/register.png')}}" alt="">
            </div>
            <div class="col-md-8 pt-15">
                <form id="business_register_form" action="{{ route('front.business.register.save') }}" method="POST" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('bussiness.form')
                </form>
            </div>
        </div>
    </div>


    @include('layouts.partials.front_footer')


    <!-- jQuery library -->
    <script src="{{asset('public/backend/node_modules/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('public/backend/node_modules/popper.js/dist/umd/popper.js')}}"></script>
    <script src="{{asset('public/backend/node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script src="{{ asset('public/backend') }}/node_modules/select2/dist/js/select2.js"></script>
    @include('sweetalert::alert')
    <script src="{{ asset('public/backend') }}/node_modules/tiny-date-picker/dist/date-range-picker.js"></script>
    <script src="{{ asset('public/backend') }}/node_modules/moment/moment.js"></script>
    <script src="{{ asset('public/backend') }}/node_modules/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="{{ asset('public/front') }}/steps/build/jquery.steps.js"></script>
    <script src="{{ asset('public/backend') }}/dist/js/ace.js"></script>
    @include('bussiness.en_lang')
    {{-- @include('bussiness.login') --}}
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if ($('#business_register_form').length) {
                var form = $('#business_register_form').show();
                form.steps({
                    headerTag: 'h3',
                    bodyTag: 'fieldset',
                    transitionEffect: 'slideLeft',
                    labels: {
                        finish: 'Resister',
                        next: 'Next',
                        previous: 'Previous',
                    },
                    onStepChanging: function(event, currentIndex, newIndex) {
                       // Allways allow previous action even if the current form is not valid!
                        if (currentIndex > newIndex) {
                            return true;
                        }
                        //Needed in some cases if the user went back (clean up)
                        if (currentIndex < newIndex) {
                            // To remove error styles
                            form.find('.body:eq(' + newIndex + ') label.error').remove();
                            form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                        }
                        form.validate().settings.ignore = ':disabled,:hidden';
                        return form.valid();
                    },
                    onFinishing: function(event, currentIndex) {
                        form.validate().settings.ignore = ':disabled';
                        return form.valid();
                    },
                    onFinished: function(event, currentIndex) {
                        // alert('submit');
                        form.submit();
                    },
                })
            }
            // var TinyDatePicker = DateRangePicker.TinyDatePicker;
            // TinyDatePicker('#currency_id', {
            //     mode: 'dp-below',
            // });
            $('#start_date').datetimepicker({
                format: 'DD/MM/YYYY',
                icons: {
                    time: 'far fa-clock text-green-d1 text-120',
                    date: 'far fa-calendar text-blue-d1 text-120',
                    up: 'fa fa-chevron-up text-secondary',
                    down: 'fa fa-chevron-down text-secondary',
                    previous: 'fa fa-chevron-left text-secondary',
                    next: 'fa fa-chevron-right text-secondary',
                    today: 'far fa-calendar-check text-purple-d1 text-120',
                    clear: 'fa fa-trash-alt text-orange-d2 text-120',
                    close: 'fa fa-times text-danger text-120'
                },
                // sideBySide: true,
                toolbarPlacement: "top",
                allowInputToggle: true,
                // showClose: true,
                // showClear: true,
                showTodayButton: true,
                //"format": "HH:mm:ss"
            });
            $('#start_date').on('dp.show', function() {
                $(this).find('.collapse.in').addClass('show')
                $(this).find('.table-condensed').addClass('table table-borderless')
                $(this).find('[data-action][title]').tooltip() // enable tooltip
            });
            $('#business_logo').aceFileInput({
                // btnChooseClass: 'bgc-grey-l2 pt-15 px-2 my-1px mr-1px',
                // btnChooseText: 'SELECT FILE',
                // placeholderText: 'NO FILE YET',
                // placeholderIcon: '<i class="fa fa-upload bgc-warning-m1 text-white w-4 py-2 text-center"></i>'
            });
            // select2
            $('.select2').select2({
                allowClear: true,
                dropdownParent: $('#select2-parent'),
            });

            $('#business_register_form').validate({
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.hasClass('input-icheck') && element.parent().hasClass('icheckbox_square-blue')) {
                        error.insertAfter(element.parent().parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    name: 'required',
                    email: {
                        email: true,
                        remote: {
                            url: "{{ route('front.business.postCheckEmail') }}",
                            type: 'post',
                            data: {
                                email: function() {
                                    return $('#email').val();
                                },
                            },
                        },
                    },
                    password: {
                        required: true,
                        minlength: 5,
                    },
                    confirm_password: {
                        equalTo: '#password',
                    },
                    username: {
                        required: true,
                        minlength: 4,
                        remote: {
                            url: "{{ route('front.business.postCheckUsername') }}",
                            type: 'post',
                            data: {
                                username: function() {
                                    return $('#username').val();
                                },
                            },
                        },
                    },
                    website: {
                        url: true,
                    },
                },
                messages: {
                    name: LANG.specify_business_name,
                    password: {
                        minlength: LANG.password_min_length,
                    },
                    confirm_password: {
                        equalTo: LANG.password_mismatch,
                    },
                    username: {
                        remote: LANG.invalid_username,
                    },
                    email: {
                        remote: LANG.email_taken,
                    },
                },
            });
        });

    </script>
</body>
</html>
