    <h3></h3>
    <input type="hidden" name="language" id="language" value="{{ request()->lang }}">
    <fieldset>
        <legend>{{ __('cruds.bussiness.title_singular') }}</legend>
        <div class="form-row">
            <div class="col-md-12">
                {{-- Bussiness Name --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-12">
                            <label for="first_name">{{ __('cruds.bussiness.fields.name') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-suitcase"></i></span>
                                </div>
                                <input name="name" id="name" value="" type="text" class="form-control" placeholder="Business Name" required>
                            </div>
                            @error('name')
                                <span id="name-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Start date and Currency --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="start_date">{{ __('cruds.bussiness.fields.start_date') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input name="start_date" id="start_date" value="" type="text" class="form-control" placeholder="Start Date">
                            </div>
                            @error('start_date')
                                <span id="start_date-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>


                        <div class="form-group col-md-6">
                            <label for="currency_id">{{ __('cruds.bussiness.fields.currency') }}</label>
                            <div class="input-group" id="select2-parent">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-money-bill-alt"></i></span>
                                </div>
                                <select id="currency_id" name="currency_id" class="select2 form-control"  data-placeholder="Select Currency" required>
                                    <option value="">Select Currency</option>
                                    @foreach ($currencies as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('currency_id')
                                <span id="currency_id-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Logo and website --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="business_logo">{{ __('cruds.bussiness.fields.logo') }}</label>
                            <div>
                                <input name="business_logo" id="business_logo" type="file" class="ace-file-input" />
                            </div>
                            @error('business_logo')
                                <span id="business_logo-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="website">{{ __('cruds.bussiness.fields.website') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                </div>
                                <input name="website" id="website" value="" type="text" class="form-control" placeholder="Website">
                              </div>
                            @error('website')
                                <span id="website-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Phone and alternate phone--}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="phone">Business contact number</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                </div>
                                <input name="phone" id="phone" value="" type="text" class="form-control" placeholder="Business contact number" required>
                              </div>
                            @error('phone')
                                <span id="phone-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="alt_phone">Alternate contact number</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                </div>
                                <input name="alt_phone" id="alt_phone" value="" type="text" class="form-control" placeholder="Alternate contact number">
                              </div>
                            @error('alt_phone')
                                <span id="alt_phone-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Country and City--}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="country">Country</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                </div>
                                <input name="country" id="country" value="" type="text" class="form-control" placeholder="Country" required>
                              </div>
                            @error('country')
                                <span id="country-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">City</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                </div>
                                <input name="city" id="city" value="" type="text" class="form-control" placeholder="City" required>
                              </div>
                            @error('city')
                                <span id="city-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Zip code and Timezone--}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="zip_code">Zip Code</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
                                </div>
                                <input name="zip_code" id="zip_code" value="" type="text" class="form-control" placeholder="Zip Code" required>
                            </div>
                            @error('zip_code')
                                <span id="zip_code-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="time_zone">{{ __('cruds.bussiness.fields.time_zone') }}</label>
                            <div class="input-group tag-input-style" id="select2-parent">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                                </div>
                                <select id="time_zone" name="time_zone" class="select2 form-control " data-placeholder="Select Timezone" required>
                                    @foreach ($timezone_list as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('time_zone')
                                <span id="time_zone-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- User Type & User Role --}}
                {{-- <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-5 col-6 tag-input-style" id="select2-parent">
                            <label for="usertype">User Type</label>
                            <select name="usertype" id="usertype" class="form-control select2">
                                <option value="">Select User type</option>
                                <option value="adm" {{ isset($user) ? $user->usertype === 'adm' ? 'selected' :null : null}}>Admin</option>
                                <option value="usr" {{ isset($user) ? $user->usertype === 'usr' ? 'selected' :null : null}}>User</option>
                                <option value="agency" {{ isset($user) ? $user->usertype === 'agency' ? 'selected' :null : null}} >Agency</option>
                                <option value="customer" {{ isset($user) ? $user->usertype === 'customer' ? 'selected' :null : null}}>Customer</option>
                            </select>
                        </div>
                        <div class="form-group col-md-7 tag-input-style">
                            <label for="usertype">User Role</label>
                            <div class="row">
                                @foreach ($roles as $key => $role)
                                        <div class="mb-1 mt-1">
                                            <label>
                                                <input type="checkbox"
                                                    @foreach ($userRoles ?? [] as $rolekey => $userRole)
                                                        {{ $key == $rolekey ? 'checked' : null}}
                                                    @endforeach
                                                class="input-lg bgc-blue" name="roles[]" id="roles" value="{{ $key }}">
                                                {{ $role }}&nbsp;&nbsp;
                                            </label>
                                        </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div> --}}
            </div>
        </div>
    </fieldset>

    <h3></h3>
    <fieldset>
        <legend>@lang('cruds.bussiness.setting')</legend>
        <div class="form-row">
            <div class="col-md-12">
                {{-- Tax 1 Name and Text Number 1 Value --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="tax_label_1">{{ __('cruds.bussiness.fields.tax_label_1') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="tax_label_1" id="tax_label_1" value="" type="text" class="form-control" placeholder="GST/VAT/Other">
                            </div>
                            @error('tax_label_1')
                                <span id="tax_label_1-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tax_number_1">{{ __('cruds.bussiness.fields.tax_number_1') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="tax_number_1" id="tax_number_1" value="" type="text" class="form-control">
                            </div>
                            @error('tax_number_1')
                                <span id="tax_number_1-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Tax 2 Name and Text Number 2 Value --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="tax_label_2">{{ __('cruds.bussiness.fields.tax_label_2') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="tax_label_2" id="tax_label_2" value="" type="text" class="form-control" placeholder="GST/VAT/Other">
                            </div>
                            @error('tax_label_2')
                                <span id="tax_label_2-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tax_number_2">{{ __('cruds.bussiness.fields.tax_number_2') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="tax_number_2" id="tax_number_2" value="" type="text" class="form-control">
                            </div>
                            @error('tax_number_2')
                                <span id="tax_number_2-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Financial year start month and Stock Accounting Method --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="fy_start_month">{{ __('cruds.bussiness.fields.fy_start_month') }}</label>
                            <div class="input-group tag-input-style" id="select2-parent">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <select id="fy_start_month" name="fy_start_month" class="form-control"  data-placeholder="Click to Choose...">
                                    @foreach ($months as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('fy_start_month')
                                <span id="fy_start_month-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="accounting_method">{{ __('cruds.bussiness.fields.accounting_method') }}</label>
                            <div class="input-group tag-input-style" id="select2-parent">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calculator"></i></span>
                                </div>
                                <select id="accounting_method" name="accounting_method" class="form-control"  data-placeholder="Click to Choose...">
                                    @foreach ($accounting_methods as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('accounting_method')
                                <span id="accounting_method-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
{{-- @endif --}}

<!-- Owner Information -->
{{-- @if(empty($is_admin)) --}}
    <h3></h3>
    <fieldset>
        <legend>{{ __('cruds.bussiness.owner') }}</legend>
        <div class="form-row">
            <div class="col-md-12">
                {{-- Surename Firstname lastname --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-4">
                            <label for="surname">{{ __('cruds.user.fields.surname') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="surname" id="surname" value="" type="text" class="form-control" placeholder="Mr/Mrs/Miss">
                            </div>
                            @error('surname')
                                <span id="surname-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="first_name">{{ __('cruds.user.fields.first_name') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="first_name" id="first_name" value="" type="text" class="form-control" placeholder="First Name" required>
                            </div>
                            @error('first_name')
                                <span id="first_name-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="last_name">{{ __('cruds.user.fields.last_name') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-info"></i></span>
                                </div>
                                <input name="last_name" id="last_name" value="" type="text" class="form-control" placeholder="Last Name">
                            </div>
                            @error('last_name')
                                <span id="last_name-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Username and Email --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="username">{{ __('cruds.user.fields.username') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <input name="username" id="username" value="" type="text" class="form-control" placeholder="Username" required>
                            </div>
                            @error('username')
                                <span id="username-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">{{ __('cruds.user.fields.email') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input name="email" id="email" value="" type="text" class="form-control" placeholder="Email" required>
                            </div>
                            @error('email')
                                <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                {{-- Password and confirm password --}}
                <div class="form-group col-md-12">
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="password">{{ __('cruds.user.fields.password') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input name="password" id="password" value="" type="password" class="form-control" placeholder="Password" required>
                            </div>
                            @error('password')
                                <span id="password-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="confirm_password">{{ __('cruds.user.fields.cpassword') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input name="password_confirmation" id="confirm_password" value="" type="password" class="form-control" placeholder="Confirm Password" required>
                            </div>
                            @error('confirm_password')
                                <span id="confirm_password-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
{{-- @endif --}}
