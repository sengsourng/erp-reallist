@foreach ($users as $key =>$row)
<tr class="d-style bgc-h-default-l4">
    <td class="td-toggle-details pos-rel">
    <div class="position-lc h-95 ml-1px border-l-3 brc-purple-m1">
    </div>
    </td>
    <td class="pl-3 pl-md-4 align-middle pos-rel">
    <input type="checkbox" />
    <div class="d-n-collapsed position-lc h-95 ml-1px border-l-3 brc-purple-m1">
    </div>
    </td>
    <td>
    <span class="text-105">
        {{ $row->username }}
    </span>
    <div class="text-95 text-secondary-d1">
        {{ $row->first_name }} {{ $row->last_name }}
    </div>
    </td>

    <td class="text-grey">
        {{ $row->usertype }}
    <div>
        @foreach ($row->roles?? [] as $role)
            <span class="badge badge-sm bgc-success-d1 text-white pb-1 px-25">
                {{ $role->name  }}
            </span>
        @endforeach
    </div>
    </td>

    <td class="text-600 text-grey-d1">
        {{ $row->phone }}
    </td>

    <td class="text-grey">
        {{ $row->email }}
    </td>

    <td class="align-middle">
        <!-- action buttons -->
        <div class="d-none d-lg-flex">
            <a href="{{ route('admin.users.edit',$row->id) }}" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-success btn-a-lighter-success">
                <i class="fa fa-pencil-alt"></i>
            </a>
            <a href="javascript:deleteObject({{ $row->id }})" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-danger btn-a-lighter-danger">
                <i class="fa fa-trash-alt"></i>
            </a>
            <form id="frmDeletePost-{{ $row->id }}" style="display: none" action="{{ route('admin.users.destroy', $row->id) }}" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </div>


        <span class="d-lg-none text-nowrap">

            <a href="{{ route('admin.users.edit',$row->id) }}" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-success btn-a-lighter-success">
                <i class="fa fa-pencil-alt"></i>
            </a>
            <a href="javascript:deleteObject({{ $row->id }})" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-danger btn-a-lighter-danger">
                <i class="fa fa-trash-alt"></i>
            </a>
            <form id="frmDeletePost-{{ $row->id }}" style="display: none" action="{{ route('admin.users.destroy', $row->id) }}" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </span>
    </td>

</tr>
@endforeach


