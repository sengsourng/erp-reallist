@extends('layouts.ace_layout')
@section('pagetitle','User List')
@push('styles')

<link rel="stylesheet" type="text/css" href="{{asset('public/backend')}}/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/backend')}}/node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/backend')}}/views/pages/table-datatables/@page-style.css">

@endpush
@section('content')

    <div class="page-content container container-plus">
        <div class="page-header mb-2 pb-2 flex-column flex-sm-row align-items-start align-items-sm-center py-25 px-1">
          <h1 class="page-title text-primary-d2 text-140">
            {{ trans('cruds.user.title') }}
            <small class="page-info text-dark-m3">
              <i class="fa fa-angle-double-right text-80"></i>
              {{ trans('global.list') }}
            </small>
          </h1>

          <div class="page-tools mt-3 mt-sm-0 mb-sm-n1">
            <!-- dataTables search box will be inserted here dynamically -->
          </div>
        </div>

        <div class="card bcard h-auto">
          <form autocomplete="off" class="border-t-3 brc-blue-m2">

            <table id="datatable" class="d-style w-100 table text-dark-m1 text-95 border-y-1 brc-black-tp11 collapsed dtr-table">
              <!-- add `collapsed` by default ... it will be removed by default -->
              <!-- thead with .sticky-nav -->
              <thead class="sticky-nav text-secondary-m1 text-uppercase text-85">
                <tr>
                  <th class="td-toggle-details border-0 bgc-white shadow-sm">
                    <i class="fa fa-angle-double-down ml-2"></i>
                  </th>

                  <th class="border-0 bgc-white pl-3 pl-md-4 shadow-sm">
                    <input type="checkbox" />
                  </th>


                  <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                    {{ trans('cruds.user.title') }}
                  </th>




                  <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                    {{ trans('cruds.user.fields.usertype') }}
                  </th>

                  <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                    {{ trans('cruds.user.fields.contact') }}
                  </th>

                  <th class="border-0 bgc-white bgc-h-yellow-l3 shadow-sm">
                    {{ trans('cruds.user.fields.email') }}
                  </th>

                  <th class="border-0 bgc-white shadow-sm w-2">
                    {{-- {{ trans('cruds.user.fields.action') }} --}}
                  </th>


                </tr>
              </thead>

              <tbody class="pos-rel">
                @include('admin.users.row')

              </tbody>
            </table>

          </form>
        </div>
      </div>
@endsection

@push('scripts')

<script src="{{asset('public/backend')}}/node_modules/datatables/media/js/jquery.dataTables.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-colreorder/js/dataTables.colReorder.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-select/js/dataTables.select.js"></script>


    <script src="{{asset('public/backend')}}/node_modules/datatables.net-buttons/js/dataTables.buttons.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-buttons/js/buttons.html5.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-buttons/js/buttons.print.js"></script>
    <script src="{{asset('public/backend')}}/node_modules/datatables.net-buttons/js/buttons.colVis.js"></script>


    <script src="{{asset('public/backend')}}/node_modules/datatables.net-responsive/js/dataTables.responsive.js"></script>
    {{-- <script src="{{asset('public/backend')}}/views/pages/table-datatables/@page-script.js"></script> --}}

    <script type="text/javascript">
        function deleteObject(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('frmDeletePost-'+id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
    <script type="text/javascript">
        jQuery(function($) {
            // highlight simple table row when selected
            function _highlight(row, checked) {
                if (checked) {
                    row.classList.add('active')
                    row.classList.add('bgc-success-l3')
                    row.classList.remove('bgc-h-default-l3')
                }
                else {
                    row.classList.remove('active')
                    row.classList.remove('bgc-success-l3')
                    row.classList.add('bgc-h-default-l3')
                }
            }

            $('#simple-table tbody tr').on('click', function(e)
            {
                var ret = false
                try {
                    // return if clicked on a .btn or .dropdown
                    ret = e.target.classList.contains('btn') || e.target.parentNode.classList.contains('btn')|| e.target.closest('.dropdown') != null
                } catch(err) {}
                if (ret) return
                var inp = this.querySelector('input')
                if(inp == null) return
                if(e.target.tagName != "INPUT") {
                    inp.checked = !inp.checked
                }
                _highlight(this, inp.checked)
            })

            $('#simple-table thead input').on('change', function()
            {
                var checked = this.checked
                $('#simple-table tbody input[type=checkbox]')
                .each(function() {
                    this.checked = checked
                    var row = $(this).closest('tr').get(0)
                    _highlight(row, checked)
                })
            })
        })
    </script>





<script>

    // $(document).ready(function() {
    //     $('#datatable').DataTable({
    //         // ordering:true,
    //         // processing: false,
    //         // serverSide: true,
    //         // "order": [[ 4, "asc" ]],
    //         language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
    //     });
    // } );

    $(document).ready(function() {
    // jQuery(function($) {
      var tableId = '#datatable'

      var tableHead = document.querySelector('.sticky-nav')
      tableHead.addEventListener('sticky-change', function(e) {
        // when  thead becomes sticky, add is-stuck class to it (which adds a border-bottom to it)
        this.classList.toggle('is-stuck', e.detail.isSticky)
      })



      $.extend(true, $.fn.dataTable.defaults, {
        dom: "<'row'<'col-12 col-sm-6'l><'col-12 col-sm-6 text-right table-tools-col'f>>" +
          "<'row'<'col-12'tr>>" +
          "<'row'<'col-12 col-md-5'i><'col-12 col-md-7'p>>",
        renderer: 'bootstrap'
      })

      var $_table = $(tableId).DataTable({
        // language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        responsive: true,
        // language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        /** optional scrolling **/
        // scrollY: "300px",
        // scrollCollapse: true,



        colReorder: {
          //disable column reordering for first and last columns
          fixedColumnsLeft: 1,
          fixedColumnsRight: 1
        },

        // sDom: 'BRfrtlip',

        classes: {
          sLength: "dataTables_length text-left w-auto",
        },


        buttons: {
          dom: {
            button: {
              className: 'btn' //remove the default 'btn-secondary'
            },
            container: {
              className: 'dt-buttons btn-group bgc-white-tp2 text-right w-auto'
            }
          },

          buttons: [{
              "extend": "colvis",
              "text": "<i class='far fa-eye text-125 text-dark-m2'></i> <span class='d-none'>{{trans('global.show')}}/hide columns</span>",
              "className": "btn-light-default btn-bgc-white btn-h-outline-primary btn-a-outline-primary",
              columns: ':not(:first)'
            },

            {
              "extend": "copy",
              "text": "<i class='far fa-copy text-125 text-purple'></i> <span class='d-none'>Copy to clipboard</span>",
              "className": "btn-light-default btn-bgc-white btn-h-outline-primary btn-a-outline-primary"
            },

            {
              "extend": "csv",
              "text": "<i class='fa fa-database text-125 text-success-m1'></i> <span class='d-none'>Export to CSV</span>",
              "className": "btn-light-default btn-bgc-white btn-h-outline-primary btn-a-outline-primary"
            },

            {
              "extend": "print",
              "text": "<i class='fa fa-print text-125 text-orange-d1'></i> <span class='d-none'>Print</span>",
              "className": "btn-light-default btn-bgc-white  btn-h-outline-primary btn-a-outline-primary",
              autoPrint: true,
              message: '{{trans('global.users')}}'
            }
          ]
        },


        // first and last column are not sortable
        columnDefs: [{
            orderable: false,
            className: null,
            targets: 0
          },
          {
            orderable: false,
            className: null,
            targets: 1
          },
          {
            orderable: false,
            className: null,
            targets: -1
          },
          {
            responsivePriority: 1,
            targets: 2
          }
        ],


        // multiple row selection
        select: {
          style: 'multis'
        },

        // no specific initial ordering
        order: [],

        language: {
          search: '<i class="fa fa-search pos-abs mt-2 pt-3px ml-25 text-blue-m2"></i>',
          searchPlaceholder: " {{trans('global.search')}} {{trans('global.users')}}..."
        }
      })


      // specify position of table buttons
      $('.table-tools-col')
        .append($_table.buttons().container())
        // move searchbox into table header
        .find('.dataTables_filter').appendTo('.page-tools').find('input').addClass('pl-45 radius-round').removeClass('form-control-sm')
        // and add a "+" button
        // .end().append('<button data-rel="tooltip" type="button" class="btn radius-round btn-outline-primary border-2 btn-sm ml-2" title="Add New User"><i class="fa fa-plus"></i></button>')
        .end().append('<a data-rel="tooltip" href="{{ route('admin.users.create') }}" class="btn radius-round btn-outline-primary border-2 btn-sm ml-2" title="Add New User"><i class="fa fa-plus"></i></a>')



      // helper methods to add/remove bgc-h-* class when selecting/deselecting rows
      var _highlightSelectedRow = function(row) {
        row.querySelector('input[type=checkbox]').checked = true
        row.classList.add('bgc-success-l3')
        row.classList.remove('bgc-h-default-l4')
      }
      var _unhighlightDeselectedRow = function(row) {
        row.querySelector('input[type=checkbox]').checked = false
        row.classList.remove('bgc-success-l3')
        row.classList.add('bgc-h-default-l4')
      }

      // listen to select/deselect event to highlight rows
      $_table
        .on('select', function(e, dt, type, index) {
          if (type == 'row') {
            var row = $_table.row(index).node()
            _highlightSelectedRow(row)
          }
        })
        .on('deselect', function(e, dt, type, index) {
          if (type == 'row') {
            var row = $_table.row(index).node()
            _unhighlightDeselectedRow(row)
          }
        })

      // when clicking the checkbox in table header, select/deselect all rows
      $(tableId)
        .on('click', 'th input[type=checkbox]', function() {
          if (this.checked) {
            $_table.rows().select().every(function() {
              _highlightSelectedRow(this.node())
            });
          } else {
            $_table.rows().deselect().every(function() {
              _unhighlightDeselectedRow(this.node())
            })
          }
        })

      // don't select row if we click on the "show details" `plus` sign (td)
      $(tableId).on('click', 'td.dtr-control', function(e) {
        e.stopPropagation()
      })


      // add/remove bgc-h-* class to TH when soring columns
      var previousTh = null
      var toggleTH_highlight = function(th) {
        th.classList.toggle('bgc-yellow-l2')
        th.classList.toggle('bgc-h-yellow-l3')
        th.classList.toggle('text-blue-d2')
      }

      $(tableId)
        .on('click', 'th:not(.sorting_disabled)', function() {
          if (previousTh != null) toggleTH_highlight(previousTh) // unhighlight previous TH
          toggleTH_highlight(this)
          previousTh = this
        })

      // don't select row when clicking on the edit icon
      $('a[data-action=edit').on('click', function(e) {
        e.preventDefault()
        e.stopPropagation() // don't select
      })

      // add a dark border
      $('.dataTables_wrapper')

        .addClass('border-b-1 border-x-1 brc-default-l2')

        // highlight DataTable header
        // also already done in CSS, but you can use custom colors here
        .find('.row:first-of-type')
        .addClass('border-b-1 brc-default-l3 bgc-blue-l4')
        .next().addClass('mt-n1px') // move the next `.row` up by 1px to go below header's border

      // highlight DataTable footer
      $('.dataTables_wrapper')
        .find('.row:last-of-type')
        .addClass('border-t-1 brc-default-l3 mt-n1px bgc-default-l4')


      // if the table has scrollbars, add ace styling to it
      $('.dataTables_scrollBody').aceScroll({
        color: "grey"
      })


      //enable tooltips
      setTimeout(function() {
        $('.dt-buttons button')
          .each(function() {
            var div = $(this).find('span').first()
            if (div.length == 1) $(this).tooltip({
              container: 'body',
              title: div.parent().text()
            })
            else $(this).tooltip({
              container: 'body',
              title: $(this).text()
            })
          })
        $('[data-rel=tooltip').tooltip({
          container: 'body'
        })
      }, 0)

    })
  </script>


@endpush
