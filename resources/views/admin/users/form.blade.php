<div class="form-row">
    <div class="col-md-6">
        {{-- First Name and Last Name --}}
        <div class="form-group col-md-12">
            <div class="form-group row">
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="first_name" type="text" value="{{ $user->first_name??null }}" placeholder="First Name" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            First Name
                        </span>
                    </div>
                    @error('first_name')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="last_name" type="text" value="{{ $user->last_name ?? null }}" placeholder="Last Name" class="form-control form-controlcol-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Last Name
                        </span>
                    </div>
                    @error('last_name')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        {{-- User Name & Email --}}
        <div class="form-group col-md-12">
            <div class="form-group row">
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="username" type="text" value="{{ $user->username ?? null }}" placeholder="User Name" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Username
                        </span>
                    </div>
                    @error('username')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="email" type="email" value="{{ $user->email ?? null }}" placeholder="Email" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Email
                        </span>
                    </div>
                    @error('email')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        {{-- Password & Confirm Password --}}
        <div class="form-group col-md-12">
            <div class="form-group row">
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="password" type="password" value="" placeholder="Password" class="form-control col-sm-8 col-md-12 shadow-none" id="password" >
                        <span class="floating-label text-grey-m3">
                            Password
                        </span>
                    </div>
                    @error('password')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="confirm-password" type="password" value="" placeholder="Confirm Password" class="form-control col-sm-8 col-md-12 shadow-none" id="confirm-password" >
                        <span class="floating-label text-grey-m3">
                            Confirm Password
                        </span>
                    </div>
                    @error('confirm-password')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        {{-- Position & Phone --}}
        <div class="form-group col-md-12">
            <div class="form-group row">
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="position" type="text" value="{{ $user->position ?? null }}" placeholder="Position" class="form-control form-controlcol-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Position
                        </span>
                    </div>
                    @error('position')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                        <input name="phone" type="text" value="{{ $user->phone ?? null }}" placeholder="Phone" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Phone
                        </span>
                    </div>
                    @error('phone')
                        <span id="email-error" class="form-text form-error text-danger-m2 d-inline-block"><i class="form-text fa fa-exclamation-circle text-danger-m1 text-100 mr-1 ml-2"></i>{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
        {{-- User Type & User Role --}}
        <div class="form-group col-md-12">
            <div class="form-group row">
                <div class="form-group col-md-5 col-6 tag-input-style" id="select2-parent">
                    <label for="usertype">User Type</label>
                    <select name="usertype" id="usertype" class="form-control select2">
                        <option value="">Select User type</option>
                        <option value="adm" {{ isset($user) ? $user->usertype === 'adm' ? 'selected' :null : null}}>Admin</option>
                        <option value="usr" {{ isset($user) ? $user->usertype === 'usr' ? 'selected' :null : null}}>User</option>
                        <option value="agency" {{ isset($user) ? $user->usertype === 'agency' ? 'selected' :null : null}} >Agency</option>
                        <option value="customer" {{ isset($user) ? $user->usertype === 'customer' ? 'selected' :null : null}}>Customer</option>
                    </select>
                </div>
                <div class="form-group col-md-7 tag-input-style">
                    <label for="usertype">User Role</label>
                    <div class="row">
                        @foreach ($roles as $key => $role)
                                <div class="mb-1 mt-1">
                                    <label>
                                        <input type="checkbox"
                                            @foreach ($userRoles ?? [] as $rolekey => $userRole)
                                                {{ $key == $rolekey ? 'checked' : null}}
                                            @endforeach
                                        class="input-lg bgc-blue" name="roles[]" id="roles" value="{{ $key }}">
                                        {{ $role }}&nbsp;&nbsp;
                                    </label>
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="card" id="card-1" draggable="false" style="">
            <div class="card-header">
                <h5 class="card-title">
                Address Information
                </h5>
            </div><!-- /.card-header -->

            <div class="card-body p-0 collapse show" style="">
                <!-- to have smooth .card toggling, it should have zero padding -->
                <div class="p-3">
                <div class="form-group col-md-12">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="address" type="text" placeholder="Address" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Address
                        </span>
                    </div>
                    @error('address') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="form-group col-md-12">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="city" type="text" placeholder="City" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            City
                        </span>
                    </div>
                    @error('city') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="form-group col-md-12">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="postal_code" type="text" placeholder="Postal Code" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Postal Code
                        </span>
                    </div>
                    @error('postal_code') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="form-group col-md-12">
                    <div class="input-floating-label text-blue-d2 brc-blue-m1">
                    <input name="country" type="text" placeholder="Country" class="form-control form-control col-sm-8 col-md-12 shadow-none" id="id-form-field-2" >
                        <span class="floating-label text-grey-m3">
                            Country
                        </span>
                    </div>
                    @error('country') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>

                </div>
            </div><!-- /.card-body -->
            </div>
    </div>
</div>
