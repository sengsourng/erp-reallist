@extends('layouts.ace_layout')

@push('styles')

@endpush

@section('content')
<div class="page-content container container-plus">
    <div class="card">
        <div class="card-header"><h5><i class="fa fa-user-plus"></i> Add User Information</h5></div>
        <form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                @include('admin.users.form')
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Publish</button>
                <a href="{{ route('admin.users.index') }}" class="btn btn-warning">Back</a>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('.select2').select2({
            // allowClear: true,
            dropdownParent: $('#select2-parent'),
            placeholder: 'Select User Type'
        })
    </script>
@endpush
