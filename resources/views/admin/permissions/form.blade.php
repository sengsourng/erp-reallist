<div class="form-group">
    <div class="row">
        {!! Form::label('group', 'Permission Group', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
            {!! Form::text('group', null, ["placeholder" => "", "class" => "form-control border-form", "required"]) !!}
        </div>
        {!! Form::label('name', 'Permission Name', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4">
            {!! Form::text('name', null, ["placeholder" => "", "class" => "form-control border-form",  "required"]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        {!! Form::label('slug', 'Slug', ['class' => 'col-sm-1 control-label']) !!}
        <div class="col-sm-5">
            {!! Form::text('slug', null, ["placeholder" => "", "class" => "form-control border-form", "required"]) !!}
        </div>
    </div>
</div>
