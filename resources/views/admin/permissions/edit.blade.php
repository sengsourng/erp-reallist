@extends('layouts.ace_layout')

@push('styles')

@endpush

@section('content')
    {{-- <div class="row justify-content-center">
        <div class="col-md-12"> --}}
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
					{!! Form::model($permission, ['route' => ['admin.permissions.update', $permission->id], 'method' => 'POST', 'class' => 'form-horizontal',
					'id' => 'validation-form', "enctype" => "multipart/form-data"]) !!}
						{{method_field('PUT')}}
						{!! Form::hidden('id', $permission->id) !!}
                        @include('admin.permissions.form')
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Publish</button>
                            <a href="{{ route('admin.permissions.index') }}" class="btn btn-warning">Back</a>
                        </div>
                    {!! Form::close() !!}
                </div>

            </div>
        {{-- </div>
    </div> --}}
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('#name').keyup(function(e){
                var str = $('#name').val();
                str = str.replace(/\W+(?!$)/g, '-').toLowerCase();//rplace stapces with dash
                $('#slug').val(str);
                $('#slug').attr('placeholder', str);
            });
        });
    </script>
@endpush
