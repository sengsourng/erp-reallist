<div class="d-none">
    <div class="container container-plus pb-45">
      <hr class="brc-secondary-l3 mb-4" />

      <div class="row">
        <div class="col-lg-8 mx-auto">
          <div class="text-center">
            <h2 class="text-grey-d3 text-160 mb-45">And other cool features</h2>
          </div>

          <div class="row">
            <div class="col-4 offset-2">
              <ul class="list-unstyled text-120 text-white">
                <li class="mb-2 bgc-info-d2 px-35 py-25">
                  <i class="w-5 far fa-clock text-170 align-middle mr-2"></i>
                  <span class="align-middle">24/7 Support</span>
                </li>

                <li class="mb-2 bgc-info-d2 px-35 py-25">
                  <i class="w-5 fa fa-code text-170 align-middle mr-2"></i>
                  <span class="align-middle">Fully Documented</span>
                </li>

                <li class="mb-2 bgc-info-d2 px-35 py-25">
                  <i class="w-5 fa fa-dice-d6 text-170 align-middle mr-2"></i>
                  <span class="align-middle">Secure Backups</span>
                </li>
              </ul>
            </div>


            <div class="col-4">
              <ul class="list-unstyled text-120 text-white">
                <li class="mb-2 bgc-info-d2 text-white px-35 py-25">
                  <i class="w-5 fa fa-redo text-170 align-middle mr-2"></i>
                  <span class="align-middle">Regular Updates</span>
                </li>

                <li class="mb-2 bgc-info-d2 px-35 py-25">
                  <i class="w-5 fa fa-eye text-170 align-middle mr-2"></i>
                  <span class="align-middle">Maximum Privacy</span>
                </li>

                <li class="mb-2  bgc-info-d2 px-35 py-25">
                  <i class="w-5 fa fa-rocket text-170 align-middle mr-2"></i>
                  <span class="align-middle">Unmatched Speed</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
