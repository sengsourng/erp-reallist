@extends('frontend.layouts.base')
@section('pagetitle','Real List Agent and Properties')

@push('custom_css')

@endpush

@section('content')
	<!--Sliders Section-->
    <section>
        <div class="banner1  relative slider-images">
            <div class="container-fuild">
                <!-- Carousel -->
                <div class="owl-carousel testimonial-owl-carousel2 slider slider-header ">
                    <div class="item cover-image" data-image-src="">
                        <img  alt="first slide" src="{{asset('public/images/banners/slide-1.jpg')}}" >
                    </div>
                    <div class="item">
                        <img  alt="first slide" src="{{asset('public/images/banners/silde-2.jpg')}}" >
                    </div>
                    <div class="item">
                        <img  alt="first slide" src="{{asset('public/images/banners/slide-3.jpg')}}" >
                    </div>
                </div>
                <div class="header-text slide-header-text mt-0 mb-0">
                    <div class="col-xl-10 col-lg-10 col-md-10 d-block mx-auto">
                        <div class="search-background bg-transparent input-field">
                            <div class="text-center text-white  mb-6 ">
                                <h1 class="mb-1 d-none d-md-block">Find Home For Sale in your Location</h1>
                                <p class="d-none d-md-block">It is a long established fact that a reader will be distracted by the readable.</p>
                            </div>
                            <div class="row">
                                <div class="col-xl-10 col-lg-12 col-md-12 d-block mx-auto">
                                    <div class="search-background bg-transparent">
                                        <div class="form row no-gutters ">
                                            <div class="form-group  col-xl-4 col-lg-3 col-md-4 mb-0 bg-white ">
                                                <input type="text" class="form-control input-lg br-tr-md-0 br-br-md-0" id="text4" placeholder="Enter Loaction, Landmark">
                                                <span><i class="fa fa-map-marker location-gps mr-1"></i></span>
                                            </div>
                                            <div class="form-group col-xl-2 col-lg-2 col-md-4 select2-lg  mb-0 bg-white">
                                                <select class="form-control select2-show-search  border-bottom-0" data-placeholder="Property Type">
                                                    <optgroup label="Categories">
                                                        <option>Property Type</option>
                                                        <option value="1">Deluxe Houses</option>
                                                        <option value="2">Modren Flats</option>
                                                        <option value="3">Apartments</option>
                                                        <option value="4">Stylish Houses</option>
                                                        <option value="5">Offices Houses</option>
                                                        <option value="6">Luxury Houses</option>
                                                        <option value="7">Nature Houses</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                            <div class="form-group col-xl-1 col-lg-1 col-md-4 select2-lg  mb-0 bg-white">
                                                <select class="form-control select2-show-search  border-bottom-0" data-placeholder="Bed Rooms">
                                                    <option value="1" selected>Bed</option>
                                                    <option value="2">1</option>
                                                    <option value="3">2</option>
                                                    <option value="4">3</option>
                                                    <option value="5">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-xl-1 col-lg-1 col-md-3 select2-lg  mb-0 bg-white">
                                                <select class="form-control select2-show-search  border-bottom-0" data-placeholder="Bath Rooms">
                                                    <option value="1" selected>Bath</option>
                                                    <option value="2">1</option>
                                                    <option value="3">2</option>
                                                    <option value="4">3</option>
                                                    <option value="5">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-xl-1 col-lg-1 col-md-3 select2-lg  mb-0 bg-white">
                                                <select class="form-control select2-show-search  border-bottom-0" data-placeholder="Min Value">
                                                    <option value="1" selected>Min</option>
                                                    <option value="2">$50</option>
                                                    <option value="3">$60</option>
                                                    <option value="4">$70</option>
                                                    <option value="5">$80</option>
                                                    <option value="5">$90</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-xl-1 col-lg-1 col-md-3 select2-lg  mb-0 bg-white">
                                                <select class="form-control select2-show-search  border-bottom-0" data-placeholder="Max Value">
                                                    <option value="1" selected>Max</option>
                                                    <option value="2">$100</option>
                                                    <option value="3">$110</option>
                                                    <option value="4">$120</option>
                                                    <option value="5">$130</option>
                                                    <option value="5">$140</option>
                                                </select>
                                            </div>
                                            <div class="col-xl-2 col-lg-3 col-md-3 mb-0">
                                                <a href="#" class="btn btn-lg btn-block btn-primary br-tl-md-0 br-bl-md-0">Search Here</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /header-text -->
            </div>
        </div>
    </section>
    <!--Sliders Section-->

    <!--Categories-->
    <section class="sptb bg-white">
        <div class="container">
            <div class="section-title center-block text-center">
                <h2>Categories</h2>
                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/h4.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Farm Houses</h4>
                                    <span class="badge badge-pill badge-primary w-15">45</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/j3.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Duplex House</h4>
                                    <span class="badge badge-pill badge-info w-15">23</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/b2.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Modren Flats</h4>
                                    <span class="badge badge-pill badge-warning w-15">48</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/v2.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Luxury Homes</h4>
                                    <span class="badge badge-pill badge-success w-15">15</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card mb-xl-0">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/f2.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Apartments</h4>
                                    <span class="badge badge-pill badge-danger w-15">12</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card mb-xl-0">
                        <div class="item-card ">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/e1.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Offices</h4>
                                    <span class="badge badge-pill badge-pink w-15">05</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card mb-xl-0">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/pe1.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">2BHK Flats</h4>
                                    <span class="badge badge-pill badge-secondary w-15">09</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="card mb-0">
                        <div class="item-card">
                            <div class="item-card-desc">
                                <a href="#"></a>
                                <div class="item-card-img">
                                    <img src="{{asset('public/images/products/co1.png')}}" alt="img" class="br-tr-7 br-tl-7">
                                </div>
                                <div class="item-card-text">
                                    <h4 class="mb-0">Budget Houses</h4>
                                    <span class="badge badge-pill badge-primary w-15">65</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/Categories-->


    <!--Featured Ads-->
    <section class="sptb bg-white">
        <div class="container">
            <div class="section-title center-block text-center">
                <h2>Featured Properties</h2>
                <p>All featured properties listing</p>
            </div>
            <div id="myCarousel2" class="owl-carousel owl-carousel-icons2">
                <!-- Wrapper for carousel items -->
                <div class="item">
                    <div class="card mb-0">
                        <div class="arrow-ribbon bg-primary">For Sale</div>
                        <div class="item-card7-imgs">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/b3.jpg')}}" alt="img" class="cover-image">
                        </div>
                        <div class="item-card7-overlaytext">
                            <a href="col-left.html" class="text-white"> Featured </a>
                            <span class="mb-0 fs-13 active"><i class="fa fa fa-heart"></i></span>
                        </div>
                        <div class="card-body">
                            <div class="item-card7-desc">
                                <div class="item-card7-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">Deluxe Flat</h4></a>
                                    <p class=""><i class="icon icon-location-pin text-muted mr-1"></i> New York, NY 10012, US </p>
                                </div>
                                <ul class="item-cards7-ic mb-0">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
                                </ul>
                                <h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex mb-0">
                                <span class="fs-12"><i class="icon icon-event mr-2 mt-1"></i>Jul 10 2019 </span>
                                <div class="ml-auto">
                                    <a href="#" class="" data-toggle="tooltip" data-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="arrow-ribbon bg-purple">For Buy</div>
                        <div class="item-card7-imgs">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/b3.jpg')}}" alt="img" class="cover-image">
                        </div>
                        <div class="item-card7-overlaytext">
                            <a href="col-left.html" class="text-white">Featured</a>
                            <span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
                        </div>
                        <div class="card-body">
                            <div class="item-card7-desc">
                                <div class="item-card7-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">2BHk Deluxe Flat</h4></a>
                                    <p class=""><i class="icon icon-location-pin text-muted mr-1"></i> New York, NY 10012, US </p>
                                </div>
                                <ul class="item-cards7-ic mb-0">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
                                </ul>
                                <h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex mb-0">
                                <span class="fs-12"><i class="icon icon-event mr-2 mt-1"></i>Jul 05 2019 </span>
                                <div class="ml-auto">
                                    <a href="#" class="" data-toggle="tooltip" data-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="arrow-ribbon bg-secondary">For Rent</div>
                        <div class="item-card7-imgs">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/b3.jpg')}}" alt="img" class="cover-image">
                        </div>
                        <div class="item-card7-overlaytext">
                            <a href="col-left.html" class="text-white">Featured</a>
                            <span class="mb-0 fs-13 active"><i class="fa fa fa-heart"></i></span>
                        </div>
                        <div class="card-body">
                            <div class="item-card7-desc">
                                <div class="item-card7-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">House For Sale</h4></a>
                                    <p class=""><i class="icon icon-location-pin text-muted mr-1"></i> New York, NY 10012, US </p>
                                </div>
                                <ul class="item-cards7-ic mb-0">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
                                </ul>
                                <h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex mb-0">
                                <span class="fs-12"><i class="icon icon-event mr-2 mt-1"></i>June 29 2019 </span>
                                <div class="ml-auto">
                                    <a href="#" class="" data-toggle="tooltip" data-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="arrow-ribbon bg-primary">For Sale</div>
                        <div class="item-card7-imgs">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/b3.jpg')}}" alt="img" class="cover-image">
                        </div>
                        <div class="item-card7-overlaytext">
                            <a href="col-left.html" class="text-white"> Featured</a>
                            <span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
                        </div>
                        <div class="card-body">
                            <div class="item-card7-desc">
                                <div class="item-card7-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">Office For Rent</h4></a>
                                    <p class=""><i class="icon icon-location-pin text-muted mr-1"></i> New York, NY 10012, US </p>
                                </div>
                                <ul class="item-cards7-ic mb-0">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
                                </ul>
                                <h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex mb-0">
                                <span class="fs-12"><i class="icon icon-event mr-2 mt-1"></i>June 25 2019 </span>
                                <div class="ml-auto">
                                    <a href="#" class="" data-toggle="tooltip" data-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item sold-out">
                    <div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
                    <div class="card mb-0">
                        <div class="arrow-ribbon bg-secondary">For Rent</div>
                        <div class="item-card7-imgs">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/b3.jpg')}}" alt="img" class="cover-image">
                        </div>
                        <div class="item-card7-overlaytext">
                            <a href="col-left.html" class="text-white">Featured</a>
                            <span class="mb-0 fs-13"><i class="fa fa fa-heart-o"></i></span>
                        </div>
                        <div class="card-body">
                            <div class="item-card7-desc">
                                <div class="item-card7-text ">
                                    <a href="col-left.html" class="text-dark"><h4 class="">Modern House For Sale</h4></a>
                                    <p class=""><i class="icon icon-location-pin text-muted mr-1"></i> New York, NY 10012, US </p>
                                </div>
                                <ul class="item-cards7-ic mb-0">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
                                </ul>
                                <h5 class="font-weight-bold mb-0">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex mb-0">
                                <span class="fs-12"><i class="icon icon-event mr-2 mt-1"></i>June 19 2019 </span>
                                <div class="ml-auto">
                                    <a href="#" class="" data-toggle="tooltip" data-placement="top" title="Share Property"><i class="icon icon-share text-muted"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!--/Featured Ads-->

    <!--Statistics-->
    <section>
        <div class="about-1 cover-image sptb pt-7 pb-7 bg-background-color" data-image-src="{{asset('public/images/banners/banner4.jpg')}}">
            <div class="content-text mb-0 text-white info">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-lg-3 col-md-6 mt-3 mb-3">
                            <div class="counter-status md-mb-0">
                                <h2 class="counter counter-1 mb-3">+786</h2>
                                <h5>Total Agents</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mt-3 mb-3">
                            <div class="counter-status md-mb-0">
                                <h2 class="counter counter-1 mb-3">+1765</h2>
                                <h5>Total Sales</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mt-3 mb-3">
                            <div class="counter-status md-mb-0">
                                <h2 class="counter counter-1 mb-3">+846</h2>
                                <h5>Total Projects</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mt-3 mb-3">
                            <div class="counter-status">
                                <h2 class="counter counter-1 mb-3">+7253</h2>
                                <h5>Happy Customers</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/Statistics-->

    <!--Testimonials-->
    <section class="sptb position-relative">
        <div class="container">
            <div class="section-title center-block text-center">
                <h1 class="position-relative">Testimonials</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel" class="owl-carousel testimonial-owl-carousel">
                        <div class="item text-center">
                            <div class="row">
                                <div class="col-xl-8 col-md-12 d-block mx-auto">
                                    <div class="">
                                        <div class="owl-controls clickable">
                                            <div class="owl-pagination">
                                                <div class="owl-page active">
                                                    <span class=""></span>
                                                </div>
                                                <div class="owl-page ">
                                                    <span class=""></span>
                                                </div>
                                                <div class="owl-page">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="">
                                            <i class="fa fa-quote-left "></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in cillum dolore eu fugiat nulla pariatur.
                                        </p>
                                        <h3 class="title">Elizabeth</h3>
                                        <div class="rating-stars mb-3">
                                            <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value" value="4">
                                            <div class="rating-stars-container">
                                                <div class="rating-star sm ">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <div class="rating-star sm ">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <div class="rating-star sm ">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <div class="rating-star sm">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <div class="rating-star sm">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="testimonial.html" class="btn btn-primary btn-lg">View all Testimonials</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item text-center">
                            <div class="row">
                                <div class="col-xl-8 col-md-12 d-block mx-auto">
                                    <div class="">
                                        <div class="owl-controls clickable">
                                            <div class="owl-pagination">
                                                <div class="owl-page ">
                                                    <span class=""></span>
                                                </div>
                                                <div class="owl-page active">
                                                    <span class=""></span>
                                                </div>
                                                <div class="owl-page">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <p class=""><i class="fa fa-quote-left"></i> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Nor again is there anyone who loves or pursues obtain pain of itself, because laboriosam ex ea commodi consequatur. </p>
                                        <div class="testimonia-data">
                                            <h3 class="title">williamson</h3>
                                            <div class="rating-stars mb-3">
                                                <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
                                                <div class="rating-stars-container">
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="testimonial.html" class="btn btn-primary btn-lg">View all Testimonials</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item text-center">
                            <div class="row">
                                <div class="col-xl-8 col-md-12 d-block mx-auto">
                                    <div class="">
                                        <div class="owl-controls clickable">
                                            <div class="owl-pagination">
                                                <div class="owl-page ">
                                                    <span class=""></span>
                                                </div>
                                                <div class="owl-page">
                                                    <span class=""></span>
                                                </div>
                                                <div class="owl-page active">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <p class=""><i class="fa fa-quote-left"></i> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum exercitation ullamco laboris nisi.</p>
                                        <div class="testimonia-data">
                                            <h3 class="title">Sophie Carr</h3>
                                            <div class="rating-stars mb-3">
                                                <input type="number" readonly="readonly" class="rating-value star" name="rating-stars-value"  value="3">
                                                <div class="rating-stars-container">
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-star sm">
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="testimonial.html" class="btn btn-primary btn-lg">View all Testimonials</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/Testimonials-->

    <!--Latest Ads-->
    <section class="sptb bg-white">
        <div class="container">
            <div class="section-title center-block text-center">
                <h2>Latest Properties</h2>
                <p>These are the last listing properties </p>
            </div>
            <div id="myCarousel1" class="owl-carousel owl-carousel-icons2">

                <div class="item">
                    <div class="card mb-0">
                        <div class="item-card2-img">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/b1.jpg')}}" alt="img" class="cover-image">
                            <div class="tag-text">
                                <span class="bg-danger tag-option">For Rent </span>
                                <span class="bg-pink tag-option">Hot</span>
                            </div>
                        </div>
                        <div class="item-card2-icons">
                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                        </div>
                        <div class="card-body">
                            <div class="item-card2">
                                <div class="item-card2-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">Office Rooms</h4></a>
                                    <p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
                                    <h5 class="font-weight-bold mb-3">$25,784 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                                </div>
                                <ul class="item-card2-list">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 256 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 8 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 4 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 4 Car</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="footerimg d-flex mt-0 mb-0">
                                <div class="d-flex footerimg-l mb-0">
                                    <img src="{{asset('public/images/faces/male/8.jpg')}}" alt="image" class="avatar brround  mr-2">
                                    <h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Joan Hunter<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
                                </div>
                                <div class="mt-2 footerimg-r ml-auto">
                                    <small class="text-muted">2 day ago</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                        <div class="item-card2-img">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/v1.jpg')}}" alt="img" class="cover-image">
                            <div class="tag-text"><span class="bg-danger tag-option">For Sale </span></div>
                        </div>
                        <div class="item-card2-icons">
                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                        </div>
                        <div class="card-body">
                            <div class="item-card2">
                                <div class="item-card2-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">Apartments</h4></a>
                                    <p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
                                    <h5 class="font-weight-bold mb-3">$89,005 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                                </div>
                                <ul class="item-card2-list">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 700 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 20 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 10 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 10 Car</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="footerimg d-flex mt-0 mb-0">
                                <div class="d-flex footerimg-l mb-0">
                                    <img src="{{asset('public/images/faces/female/19.jpg')}}" alt="image" class="avatar brround  mr-2">
                                    <h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Elizabeth<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
                                </div>
                                <div class="mt-2 footerimg-r ml-auto">
                                    <small class="text-muted">50 mins ago</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item sold-out">
                    <div class="ribbon sold-ribbon ribbon-top-left text-danger"><span class="bg-danger">Sold Out</span></div>
                    <div class="card mb-0">
                        <div class="power-ribbon power-ribbon-top-left text-warning"><span class="bg-warning"><i class="fa fa-bolt"></i></span></div>
                        <div class="item-card2-img">
                            <a href="col-left.html"></a>
                            <img src="{{asset('public/images/products/products/v1.jpg')}}" alt="img" class="cover-image">
                            <div class="tag-text">
                                <span class="bg-danger tag-option">For Sale </span>
                                <span class="bg-pink tag-option">New</span>
                            </div>
                        </div>
                        <div class="item-card2-icons">
                            <a href="#" class="item-card2-icons-l bg-primary"> <i class="fa fa-home"></i></a>
                            <a href="#" class="item-card2-icons-r bg-secondary"><i class="fa fa fa-heart-o"></i></a>
                        </div>
                        <div class="card-body">
                            <div class="item-card2">
                                <div class="item-card2-text">
                                    <a href="col-left.html" class="text-dark"><h4 class="">Duplex House</h4></a>
                                    <p class="mb-2"><i class="fa fa-map-marker text-danger mr-1"></i> Preston Street Wichita , USA </p>
                                    <h5 class="font-weight-bold mb-3">$23,789 <span class="fs-12  font-weight-normal">Per Month</span></h5>
                                </div>
                                <ul class="item-card2-list">
                                    <li><a href="#"><i class="fa fa-arrows-alt text-muted mr-1"></i> 300 Sqft</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bed text-muted mr-1"></i> 4 Beds</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-bath text-muted mr-1"></i> 3 Bath</a></li>
                                    <li><a href="#" class="icons"><i class="fa fa-car text-muted mr-1"></i> 1 Car</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="footerimg d-flex mt-0 mb-0">
                                <div class="d-flex footerimg-l mb-0">
                                    <img src="{{asset('public/images/faces/female/18.jpg')}}" alt="image" class="avatar brround  mr-2">
                                    <h5 class="time-title text-muted p-0 leading-normal mt-1 mb-0">Boris Nash<i class="si si-check text-success fs-12 ml-1" data-toggle="tooltip" data-placement="top" title="verified"></i></h5>
                                </div>
                                <div class="mt-2 footerimg-r ml-auto">
                                    <small class="text-muted">12 mins ago</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!--Latest Ads-->

    <!-- News -->
    <section class="sptb">
        <div class="container">
            <div class="section-title center-block text-center">
                <h2>Recent Posts</h2>
                <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            </div>
            <div id="defaultCarousel" class="owl-carousel Card-owlcarousel owl-carousel-icons">
                <div class="item">
                    <div class="card mb-0">
                        <div class="item7-card-img">
                            <a href="blog-details.html"></a>
                            <img src="{{asset('public/images/products/products/ed1.jpg')}}" alt="img" class="cover-image">
                            <div class="item7-card-text">
                                <span class="badge badge-info">Farm House</span>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="item7-card-desc d-flex mb-2">
                                <a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>July-03-2019</a>
                                <div class="ml-auto">
                                    <a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>4 Comments</a>
                                </div>
                            </div>
                            <a href="blog-details.html" class="text-dark"><h4 class="fs-20">Luxury Flat For Rent</h4></a>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
                            <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="{{asset('public/images/faces/male/5.jpg')}}" class="avatar brround avatar-md mr-3" alt="avatar-img">
                                <div>
                                    <a href="profile.html" class="text-default">Joanne Nash</a>
                                    <small class="d-block text-muted">1 day ago</small>
                                </div>
                                <div class="ml-auto text-muted">
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="item7-card-img">
                            <a href="blog-details.html"></a>
                            <img src="{{asset('public/images/products/products/j2.jpg')}}" alt="img" class="cover-image">
                            <div class="item7-card-text">
                                <span class="badge badge-primary">Luxury Room</span>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="item7-card-desc d-flex mb-2">
                                <a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>June-03-2019</a>
                                <div class="ml-auto">
                                    <a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>2 Comments</a>
                                </div>
                            </div>
                            <a href="blog-details.html" class="text-dark"><h4 class="fs-20">Deluxe Flat For Sale</h4></a>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
                            <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="{{asset('public/images/faces/male/7.jpg')}}" class="avatar brround avatar-md mr-3" alt="avatar-img">
                                <div>
                                    <a href="profile.html" class="text-default">Tanner Mallari</a>
                                    <small class="d-block text-muted">2 days ago</small>
                                </div>
                                <div class="ml-auto text-muted">
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="item7-card-img">
                            <a href="blog-details.html"></a>
                            <img src="{{asset('public/images/products/products/ed2.jpg')}}" alt="img" class="cover-image">
                            <div class="item7-card-text">
                                <span class="badge badge-success">2BHK Flat</span>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="item7-card-desc d-flex mb-2">
                                <a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>June-19-2019</a>
                                <div class="ml-auto">
                                    <a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>8 Comments</a>
                                </div>
                            </div>
                            <a href="blog-details.html" class="text-dark"><h4 class="fs-20">Luxury Home  For Sale</h4></a>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
                            <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="{{asset('public/images/faces/female/15.jpg')}}" class="avatar brround avatar-md mr-3" alt="avatar-img">
                                <div>
                                    <a href="profile.html" class="text-default">Aracely Bashore</a>
                                    <small class="d-block text-muted">5 days ago</small>
                                </div>
                                <div class="ml-auto text-muted">
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="item7-card-img">
                            <a href="blog-details.html"></a>
                            <img src="{{asset('public/images/products/products/h1.jpg')}}" alt="img" class="cover-image">
                            <div class="item7-card-text">
                                <span class="badge badge-primary">Duplex House</span>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="item7-card-desc d-flex mb-2">
                                <a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>June-03-2019</a>
                                <div class="ml-auto">
                                    <a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>4 Comments</a>
                                </div>
                            </div>
                            <a href="blog-details.html" class="text-dark"><h4 class="font-weight-semibold">Luxury Flat For Rent</h4></a>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
                            <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="{{asset('public/images/faces/male/15.jpg')}}" class="avatar brround avatar-md mr-3" alt="avatar-img">
                                <div>
                                    <a href="profile.html" class="text-default">Royal Hamblin</a>
                                    <small class="d-block text-muted">10 days ago</small>
                                </div>
                                <div class="ml-auto text-muted">
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="item7-card-img">
                            <a href="blog-details.html"></a>
                            <img src="{{asset('public/images/products/products/j3.jpg')}}" alt="img" class="cover-image">
                            <div class="item7-card-text">
                                <span class="badge badge-pink">Budget House</span>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="item7-card-desc d-flex mb-2">
                                <a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>May-28-2019</a>
                                <div class="ml-auto">
                                    <a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>2 Comments</a>
                                </div>
                            </div>
                            <a href="blog-details.html" class="text-dark"><h4 class="font-weight-semibold">Home  For Sale</h4></a>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
                            <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="{{asset('public/images/faces/female/5.jpg')}}" class="avatar brround avatar-md mr-3" alt="avatar-img">
                                <div>
                                    <a href="profile.html" class="text-default">Rosita Chatmon</a>
                                    <small class="d-block text-muted">10 days ago</small>
                                </div>
                                <div class="ml-auto text-muted">
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card mb-0">
                        <div class="item7-card-img">
                            <a href="blog-details.html"></a>
                            <img src="{{asset('public/images/products/products/ed4.jpg')}}" alt="img" class="cover-image">
                            <div class="item7-card-text">
                                <span class="badge badge-success">3BHK Flats</span>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="item7-card-desc d-flex mb-2">
                                <a href="#" class="text-muted"><i class="fa fa-calendar-o text-muted mr-2"></i>May-19-2019</a>
                                <div class="ml-auto">
                                    <a href="#" class="text-muted"><i class="fa fa-comment-o text-muted mr-2"></i>8 Comments</a>
                                </div>
                            </div>
                            <a href="blog-details.html" class="text-dark"><h4 class="font-weight-semibold">Luxury Home  For Sale</h4></a>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip commodo consequat </p>
                            <div class="d-flex align-items-center pt-2 mt-auto">
                                <img src="{{asset('public/images/faces/male/6.jpg')}}" class="avatar brround avatar-md mr-3" alt="avatar-img">
                                <div>
                                    <a href="profile.html" class="text-default">Loyd Nolf</a>
                                    <small class="d-block text-muted">15 days ago</small>
                                </div>
                                <div class="ml-auto text-muted">
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-heart mr-1"></i></a>
                                    <a href="javascript:void(0)" class="icon d-none d-md-inline-block ml-3"><i class="fa fa-thumbs-o-up"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--News -->

@endsection

@push('custom_js')

@endpush
