<div class="bgc-primary-l3">
    <div class="container container-plus py-45">
      <div class="row">
        <div class="col-12 col-lg-9 mx-auto">
          <h3 class="text-center text-dark-m3 mb-4 mb-md-5" data-aos="fade-up">
            Testimonials
          </h3>

          <div class="row justify-content-center">
            <div class="col-12 col-md-4 mb-2 mb-md-0 minh-100" data-aos="fade-up" data-aos-delay="50">
              <div class="text-center bgc-white p-45 radius-1 shadow-md minh-100 d-flex flex-column">
                <div class="mt-md-n2 mb-3">
                  <img alt="Customer avatar" src="{{asset('public/backend/assets/image/avatar/avatar1.jpg')}}" class="mt-md-n5 radius-round border-2 brc-default-m1 p-2px shadow-md" />
                </div>

                <h4 class="mb-15">
                  Perfect timing
                </h4>

                <div>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-grey-l3"></i>
                </div>

                <p class="mt-2 text-dark-tp3 text-95">
                  Lorem ipsum dolor sit amet.
                  <br />
                  Adipiscing elit nam at lacus at augue aliquet posuere.
                  <br />
                  Eliquam fringilla elementum varius.
                </p>

                <p class="text-600 mb-0 mt-auto">
                  Jason Wells
                </p>
              </div>
            </div><!-- /.col -->


            <div class="col-12 col-md-4 mb-2 mb-md-0 minh-100" data-aos="fade-up" data-aos-delay="150">
              <div class="text-center bgc-white p-45 radius-1 shadow-md minh-100 d-flex flex-column">
                <div class="mt-md-n2 mb-3">
                  <img alt="Customer avatar" src="{{asset('public/backend/assets/image/avatar/avatar2.jpg')}}" class="mt-md-n5 radius-round border-2 brc-default-m1 p-2px shadow-md" />
                </div>

                <h4 class="mb-15">
                  Cost effective
                </h4>

                <div>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                </div>

                <p class="mt-2 text-dark-tp3 text-95">
                  Lorem ipsum dolor sit amet.
                  <br />
                  Eliquam fringilla elementum varius.
                </p>

                <p class="text-600 mb-0 mt-auto">
                  Alex Wizard
                </p>
              </div>
            </div><!-- /.col -->


            <div class="col-12 col-md-4 mb-2 mb-md-0 minh-100" data-aos="fade-up" data-aos-delay="250">
              <div class="text-center bgc-white p-45 radius-1 shadow-md minh-100 d-flex flex-column">
                <div class="mt-md-n2 mb-3">
                  <img alt="Customer avatar" src="{{asset('public/backend/assets/image/avatar/avatar3.jpg')}}" class="mt-md-n5 radius-round border-2 brc-default-m1 p-2px shadow-md" />
                </div>

                <h4 class="mb-15">
                  Blazingly fast
                </h4>

                <div>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-orange"></i>
                  <i class="fa fa-star text-grey-l3"></i>
                </div>

                <p class="mt-2 text-dark-tp3 text-95">
                  Adipiscing elit nam at lacus at augue aliquet posuere.
                  <br />
                  Eliquam fringilla elementum varius.
                </p>

                <p class="text-600 mb-0 mt-auto">
                  Alice Summer
                </p>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.col -->
      </div><!-- /.row -->

    </div><!-- /.container -->
  </div>
