<div class="bgc-white">
    <div class="container container-plus py-35">
      <hr class="brc-secondary-l3 my-4 my-lg-5" />

      <div class="row">
        <div class="col-12 col-lg-8 mx-auto text-center">
          <h3 class="text-dark-l1 mb-4 mt-2 text-180" data-aos="fade-up">
            So why is everybody choosing us?
          </h3>

          <ul class="d-flex flex-column flex-sm-row d-md-inline-flex nav nav-fill nav-tabs nav-tabs-static mx-n3 mx-md-0 py-1 px-1 px-md-0" role="tablist">
            <li data-aos="fade-up" data-aos-delay="200" class="nav-item mr-2px mb-1">
              <a data-toggle="tab" href="#reason1" class="active btn border-1 radius-round py-2 px-35 btn-outline-secondary btn-h-light-green btn-a-green" role="tab" aria-selected="true">
                State of the art
              </a>
            </li>

            <li data-aos="fade-up" data-aos-delay="300" class="nav-item mx-2px mb-1">
              <a data-toggle="tab" href="#reason2" class="btn border-1 radius-round py-2 px-35 btn-outline-secondary btn-h-light-orange btn-a-orange" role="tab" aria-selected="false">
                Responsive
              </a>
            </li>

            <li data-aos="fade-up" data-aos-delay="400" class="nav-item mx-2px mb-1">
              <a data-toggle="tab" href="#reason3" class="btn border-1 radius-round py-2 px-35 btn-outline-secondary btn-h-light-blue btn-a-blue" role="tab" aria-selected="false">
                Ease of use
              </a>
            </li>
          </ul><!-- /.nav-tabs -->


          <div data-aos="fade-up" data-aos-delay="300" class="tab-content tab-sliding text-left border-0 mt-5 px-0">

            <div class="tab-pane px-3 active show" id="reason1">
              <div class="d-flex flex-column align-items-center flex-md-row align-items-md-start">
                <div class="flex-grow-1 text-dark-tp3 mt-4 mt-md-0">
                  <h3 class="text-140 text-secondary-d3 text-center text-md-left mt-md-4">
                    Our tech is bleeding edge and reliable!
                  </h3>

                  <ul class="list-unstyled ml-3 mt-4">
                    <li class="mb-3">
                      <i class="fa fa-caret-right text-125 text-green mr-1"></i>
                      Onec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo.
                    </li>
                    <li class="mb-3">
                      <i class="fa fa-caret-right text-125 text-green mr-1"></i>
                      Justo odio, dapibus ac facilisis in, egestas eget quam.
                    </li>
                    <li>
                      <i class="fa fa-caret-right text-125 text-green mr-1"></i>
                      Donec id elit non mi porta gravida at eget metus.
                    </li>
                  </ul>
                </div>

                <div class="d-flex flex-nowrap order-first order-md-last ml-md-4">
                  <!-- the 3 mobile dashboard images -->
                  <div class="mx-n2">
                    <div class="radius-2 shadow-sm border-1 p-2px brc-default-l2 bgc-white">
                      <div class="overflow-hidden radius-1">
                        <img alt="Product image 1" src="{{asset('public/backend/assets/image/landing/mobile-dash2.png')}}" height="220" />
                      </div>
                    </div>
                  </div>

                  <div class="mx-n2 pos-rel mt-n25">
                    <div class="radius-2 shadow-sm border-1 p-2px brc-default-l2 bgc-white">
                      <div class="overflow-hidden radius-1">
                        <img alt="Product image 2" src="{{asset('public/backend/assets/image/landing/mobile-dash1.png')}}" height="220" />
                      </div>
                    </div>
                  </div>

                  <div class="mx-n2">
                    <div class="radius-2 shadow-sm border-1 p-2px brc-default-l2 bgc-white">
                      <div class="overflow-hidden radius-1">
                        <img alt="Product image 3" src="{{asset('public/backend/assets/image/landing/mobile-dash3.png')}}" height="220" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.tab-pane -->


            <div class="tab-pane px-3" id="reason2">
              <div class="d-flex flex-column align-items-center flex-md-row align-items-md-start">
                <div class=" mr-md-4 ">
                  <div class="overflow-hidden radius-1">
                    <img alt="Product image 4" src="{{asset('public/backend/assets/image/landing/ipad.png')}}" height="250" />
                  </div>
                </div>

                <div class="flex-grow-1 text-dark-tp3 mt-4 mt-md-0">
                  <h3 class="text-140 text-secondary-d3 text-center text-md-left mt-md-4">Fully responsive!</h3>
                  <ul class="list-unstyled ml-3 mt-4">
                    <li class="mb-3">
                      <i class="fa fa-caret-right text-125 text-orange mr-1"></i>
                      Donec id elit non mi porta gravida at eget metus.
                    </li>
                    <li class="mb-3">
                      <i class="fa fa-caret-right text-125 text-orange mr-1"></i>
                      Onec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo.
                    </li>
                    <li>
                      <i class="fa fa-caret-right text-125 text-orange mr-1"></i>
                      Justo odio, dapibus ac facilisis in, egestas eget quam.
                    </li>
                  </ul>
                </div>
              </div>
            </div><!-- /.tab-pane -->


            <div class="tab-pane px-3" id="reason3">
              <div class="d-flex flex-column align-items-center flex-md-row align-items-md-start">
                <div class="flex-grow-1 text-dark-tp3 mt-4 mt-md-0">
                  <h3 class="text-140 text-secondary-d3 text-center text-md-left mt-md-4">
                    Very easy to use!
                  </h3>

                  <ul class="list-unstyled ml-3 mt-4">
                    <li class="mb-3">
                      <i class="fa fa-caret-right text-125 text-blue mr-1"></i>
                      Onec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo.
                    </li>
                    <li class="mb-3">
                      <i class="fa fa-caret-right text-125 text-blue mr-1"></i>
                      Justo odio, dapibus ac facilisis in, egestas eget quam.
                    </li>
                    <li>
                      <i class="fa fa-caret-right text-125 text-blue mr-1"></i>
                      Donec id elit non mi porta gravida at eget metus.
                    </li>
                  </ul>
                </div>

                <div class="radius-2 ml-md-4 shadow-sm border-1 p-2px brc-default-l2 bgc-white order-first order-md-last">
                  <div class="overflow-hidden radius-1">
                    <img alt="Product image 5" src="{{asset('public/backend/assets/image/landing/layout-code.png')}}" width="320" />
                  </div>
                </div>
              </div>
            </div><!-- /.tab-pane -->
          </div><!-- /.tab-content -->

        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div>
