<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="Reallist- Bootstrap Responsive Real estate Classifieds, Dealer, Rentel, Builder and Agent Multipurpose HTML Template" name="description">
		<meta content="Spruko Technologies Private Limited" name="author">
		<meta name="keywords" content="html template, real estate websites, real estate html template, property websites, premium html templates, real estate company website, bootstrap real estate template, real estate marketplace html template, listing website template, property listing html template, real estate bootstrap template, real estate html5 template, real estate listing template, property template, property dealer website"/>

		<!-- Favicon -->
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>@yield('pagetitle','Reallist Home Page')</title>

		<!-- Bootstrap Css -->
		<link href="{{asset('public/assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css')}}" rel="stylesheet" />

		<!-- Dashboard Css -->
		<link href="{{asset('public/assets/css/style.css')}}" rel="stylesheet" />

		<!-- Font-awesome  Css -->
		<link href="{{asset('public/assets/css/icons.css')}}" rel="stylesheet"/>

		<!--Select2 Plugin -->
		<link href="{{asset('public/assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />

		<!-- Cookie css -->
		<link href="{{asset('public/assets/plugins/cookie/cookie.css')}}" rel="stylesheet">

		<!-- Owl Theme css-->
		<link href="{{asset('public/assets/plugins/owl-carousel/owl.carousel.css')}}" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="{{asset('public/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}" rel="stylesheet" />

		<!-- P-scroll bar css-->
		<link href="{{asset('public/assets/plugins/pscrollbar/perfect-scrollbar.css')}}" rel="stylesheet" />

		<!-- COLOR-SKINS -->
		<link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('public/assets/skins/color-skins/color15.css')}}" />
		<link rel="stylesheet" href="{{asset('public/assets/skins/demo.css')}}"/>

        <?php  $flag = app()->getlocale(); ?>
        <style>
             body{
                    font-family: 'Hanuman', 'serif' !important;
                }
        </style>
        @stack('custom_css')

	</head>

	<body class="main-body">

		<!--Loader-->
		<div id="global-loader">
			<img src="{{asset('public/images/loader.svg')}}" class="loader-img" alt="">
		</div>

		<!--Topbar-->
        @include('frontend.layouts.topbar')


        @yield('content')



		<!--Footer Section-->
		<section class="main-footer">
			<footer class="bg-dark-purple text-white">
				<div class="footer-main">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-md-12">
								<h6>About</h6>
								<hr class="deep-purple  accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis  exercitation ullamco laboris </p>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum .</p>
							</div>
							<div class="col-lg-2 col-md-12">
								<h6>Our Quick Links</h6>
								 <hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<ul class="list-unstyled mb-0">
									<li><a href="javascript:;">Our Team</a></li>
									<li><a href="javascript:;">Contact US</a></li>
									<li><a href="javascript:;">About</a></li>
									<li><a href="javascript:;">Luxury Rooms</a></li>
									<li><a href="javascript:;">Blog</a></li>
									<li><a href="javascript:;">Terms</a></li>
								</ul>
							</div>

							<div class="col-lg-3 col-md-12">
								<h6>Contact</h6>
								<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<ul class="list-unstyled mb-0">
									<li>
										<a href="#"><i class="fa fa-home mr-3 text-primary"></i> New York, NY 10012, US</a>
									</li>
									<li>
										<a href="#"><i class="fa fa-envelope mr-3 text-primary"></i> info12323@example.com</a></li>
									<li>
										<a href="#"><i class="fa fa-phone mr-3 text-primary"></i> + 01 234 567 88</a>
									</li>
									 <li>
										<a href="#"><i class="fa fa-print mr-3 text-primary"></i> + 01 234 567 89</a>
									</li>
								</ul>
								<ul class="list-unstyled list-inline mt-3">
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-facebook bg-facebook"></i>
									  </a>
									</li>
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-twitter bg-info"></i>
									  </a>
									</li>
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-google-plus bg-danger"></i>
									  </a>
									</li>
									<li class="list-inline-item">
									  <a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
										<i class="fa fa-linkedin bg-linkedin"></i>
									  </a>
									</li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-12">
								<h6>Subscribe</h6>
								<hr class="deep-purple  text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
								<div class="clearfix"></div>
								<div class="input-group w-100">
									<input type="text" class="form-control br-tl-3  br-bl-3 " placeholder="Email">
									<div class="input-group-append ">
										<button type="button" class="btn btn-primary br-tr-3  br-br-3"> Subscribe </button>
									</div>
								</div>
								<h6 class="mb-0 mt-5">Payments</h6>
								<hr class="deep-purple  text-primary accent-2 mb-2 mt-3 d-inline-block mx-auto">
								<div class="clearfix"></div>
								<ul class="footer-payments">
									<li class="pl-0"><a href="javascript:;"><i class="fa fa-cc-amex text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-cc-visa text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-credit-card-alt text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-cc-mastercard text-muted" aria-hidden="true"></i></a></li>
									<li><a href="javascript:;"><i class="fa fa-cc-paypal text-muted" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="bg-dark-purple text-white p-0">
					<div class="container">
						<div class="row d-flex">
							<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
								Copyright © 2019 <a href="#" class="fs-14 text-primary">Reallist</a>. Designed by <a href="#" class="fs-14 text-primary">Spruko</a> All rights reserved.
							</div>
						</div>
					</div>
				</div>
			</footer>
		</section>
		<!--Footer Section-->

		<!-- Back to top -->
		<a href="#top" id="back-to-top" ><i class="fa fa-rocket"></i></a>

		<!-- JQuery js-->
		<script src="{{asset('public/assets/js/vendors/jquery-3.2.1.min.js')}}"></script>

		<!-- Bootstrap js -->
		<script src="{{asset('public/assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js')}}"></script>
		<script src="{{asset('public/assets/plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js')}}"></script>

		<!--JQuery RealEstaterkline Js-->
		<script src="{{asset('public/assets/js/vendors/jquery.sparkline.min.js')}}"></script>

		<!-- Circle Progress Js-->
		<script src="{{asset('public/assets/js/vendors/circle-progress.min.js')}}"></script>

		<!-- Star Rating Js-->
		<script src="{{asset('public/assets/plugins/rating/jquery.rating-stars.js')}}"></script>

		<!--Owl Carousel js -->
		<script src="{{asset('public/assets/plugins/owl-carousel/owl.carousel.js')}}"></script>

		<!--Horizontal Menu-->
		<script src="{{asset('public/assets/plugins/horizontal-menu/horizontal.js')}}"></script>

		<!--Counters -->
		<script src="{{asset('public/assets/plugins/counters/counterup.min.js')}}"></script>
		<script src="{{asset('public/assets/plugins/counters/waypoints.min.js')}}"></script>
		<script src="{{asset('public/assets/plugins/counters/numeric-counter.js')}}"></script>

		<!--JQuery TouchSwipe js-->
		<script src="{{asset('public/assets/js/jquery.touchSwipe.min.js')}}"></script>

		<!--Select2 js -->
		<script src="{{asset('public/assets/plugins/select2/select2.full.min.js')}}"></script>
		<script src="{{asset('public/assets/js/select2.js')}}"></script>

		<!-- sticky Js-->
		<script src="{{asset('public/assets//js/sticky.js')}}"></script>

		<!-- Cookie js -->
		<script src="{{asset('public/assets/plugins/cookie/jquery.ihavecookies.js')}}"></script>
		<script src="{{asset('public/assets/plugins/cookie/cookie.js')}}"></script>

        <!-- Custom scroll bar Js-->
		<script src="{{asset('public/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

		<!-- P-scroll bar Js-->
		<script src="{{asset('public/assets/plugins/pscrollbar/perfect-scrollbar.js')}}"></script>
		<script src="{{asset('public/assets/plugins/pscrollbar/pscroll.js')}}"></script>

		<!-- Swipe Js-->
		<script src="{{asset('public/assets/js/swipe.js')}}"></script>

		<!-- Scripts Js-->
		<script src="{{asset('public/assets/js/owl-carousel.js')}}"></script>

		<!-- Custom Js-->
		<script src="{{asset('public/assets/js/custom.js')}}"></script>


        @stack('custom_js')
	</body>
</html>
