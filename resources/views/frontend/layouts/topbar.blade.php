<div class="header-main">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-sm-4 col-7">
                    <div class="top-bar-left d-flex">
                        <div class="clearfix">
                            <ul class="socials">
                                <li>
                                    <a class="social-icon text-dark" href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a class="social-icon text-dark" href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="social-icon text-dark" href="#"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a class="social-icon text-dark" href="#"><i class="fa fa-google-plus"></i></a>
                                </li>


                               <li>
                                @if ($flag=='en')
                                <a class="social-icon text-dark" href="{{url('lang/kh')}}">
                                    <img src="{{asset('public/images/flags/kh.png')}}" alt="" width="32" height="18">
                                    {{-- <span class="text-white"> KH</span> --}}
                                </a>
                            @endif

                            @if ($flag=='kh')
                                <a class="social-icon text-dark" href="{{url('lang/en')}}">
                                    <img src="{{asset('public/images/flags/en.png')}}" alt="" width="32" height="18">
                                    {{-- <span class="text-white"> EN</span> --}}
                                </a>
                            @endif
                               </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-8 col-5">
                    <div class="top-bar-right">
                        <ul class="custom">
                            @php
                                $userType=Auth::user()->usertype;
                            @endphp
                            @if (Auth::user())
                                <li class="dropdown">
                                    <a href="#" class="text-dark" data-toggle="dropdown"><i class="fa fa-user mr-1"></i><span> {{Auth::user()->first_name}} {{Auth::user()->last_name}}</span></a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a href="{{url($userType)}}" class="dropdown-item" >
                                            <i class="dropdown-icon icon icon-user"></i> My Profile
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i class="dropdown-icon icon icon-speech"></i> Inbox
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i class="dropdown-icon icon icon-bell"></i> Notifications
                                        </a>
                                        <a href="mydash.html" class="dropdown-item" >
                                            <i class="dropdown-icon  icon icon-settings"></i> Account Settings
                                        </a>

                                        <form method="POST" action="{{ route('logout') }}">
                                            @csrf
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                            onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                                    <i class="dropdown-icon icon icon-power"></i>   {{trans('global.logout')}}
                                                </a>
                                        </form>

                                    </div>
                                </li>

                            @else
                            <li>
                                <a href="{{route('register')}}" class="text-dark"><i class="fa fa-user mr-1"></i> <span>Register</span></a>
                            </li>
                            <li>
                                <a href="{{route('login')}}" class="text-dark"><i class="fa fa-sign-in mr-1"></i> <span>Login</span></a>
                            </li>

                            @endif






                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Duplex Houses Header -->
    <div class="horizontal-header clearfix ">
        <div class="container">
            <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
            <span class="smllogo"><img src="{{asset('public/images/brand/logo.png')}}" width="120" alt=""/></span>
            <a href="tel:245-6325-3256" class="callusbtn"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
    </div>
    <!-- /Duplex Houses Header -->

    <div class="horizontal-main bg-dark-transparent clearfix">
        <div class="horizontal-mainwrapper container clearfix">
            <div class="desktoplogo">
                <a href="index.html"><img src="{{asset('public/images/brand/logo1.png')}}" alt=""></a>
            </div>
            <div class="desktoplogo-1">
                <a href="index.html"><img src="{{asset('public/images/brand/logo1.png')}}" alt=""></a>
            </div>
            <!--Nav-->
            <nav class="horizontalMenu clearfix d-md-flex">
                <ul class="horizontalMenu-list">
                    <li aria-haspopup="true"><a class="active" href="{{url('properties')}}">Home </a></li>
                    <li aria-haspopup="true"><a href="about.html">About Us </a></li>


                    <li aria-haspopup="true"><a href="#">Properties <span class="fa fa-caret-down m-0"></span></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="#">For Buying <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                                 <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="blog-grid.html">Houses</a></li>
                                    <li aria-haspopup="true"><a href="blog-grid-right.html">Lands</a></li>
                                    <li aria-haspopup="true"><a href="blog-grid-center.html">Flats</a></li>
                                </ul>
                            </li>
                            <li aria-haspopup="true"><a href="#">For Selling <i class="fa fa-angle-right float-right mt-1 d-none d-lg-block"></i></a>
                                 <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="blog-list.html">Houses</a></li>
                                    <li aria-haspopup="true"><a href="blog-list-right.html">Lands</a></li>
                                    <li aria-haspopup="true"><a href="blog-list-center.html">Flats</a></li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#">Real Estate Agent <span class="fa fa-caret-down m-0"></span></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="col-left.html">Find Agent</a></li>
                            <li aria-haspopup="true"><a href="col-right.html">Join Agent</a></li>
                            <li aria-haspopup="true"><a href="col-right.html">Join Our Referal Network</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="contact.html"> Contact Us <span class="hmarrow"></span></a></li>
                    <li aria-haspopup="true" class="d-lg-none mt-5 pb-5 mt-lg-0">
                        <span><a class="btn btn-secondary" href="ad-posts.html">Post Property Ad</a></span>
                    </li>
                </ul>
                <ul class="mb-0">
                    <li aria-haspopup="true" class="mt-3 d-none d-lg-block top-postbtn">
                        <span><a class="btn btn-secondary ad-post " href="ad-posts.html">Post Property Ad</a></span>
                    </li>
                </ul>
            </nav>
            <!--Nav-->
        </div>
    </div>
</div>
