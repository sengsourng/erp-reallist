<nav class="navbar navbar-expand-lg navbar-darkblue" id="navbar-dark">
    <div class="navbar-inner">
      <div class="container">
        <button type="button" class="navbar-toggler btn btn-burger burger-times static collapsed d-flex d-lg-none ml-2" data-toggle="collapse" data-target="#navbarMenu1-dark" aria-controls="navbarMenu1-dark" aria-expanded="false" aria-label="Toggle navbar menu">
          <span class="bars text-white"></span>
        </button><!-- mobile navbar toggler button -->
        <div class="navbar-intro justify-content-xl-between bgc-transparent">
          <a class="navbar-brand text-white text-180" href="#">
            <i class="fa fa-home text-105 text-green-l5 mr-1"></i>
            <span>IT-Smart</span>
            {{-- <span class="text-70">Properties</span> --}}
          </a><!-- /.navbar-brand -->
          <?php  $flag = app()->getlocale(); ?>
            @if ($flag=='en')
                <a class="nav-link" href="{{url('lang/kh')}}">
                    <img src="{{asset('public/images/flags/kh.png')}}" class="img-flag" alt="" width="32" height="18">
                    {{-- <span class="text-white"> KH</span> --}}
                </a>
            @endif
            @if ($flag=='kh')
                <a class="nav-link" href="{{url('lang/en')}}">
                    <img src="{{asset('public/images/flags/en.png')}}" class="img-flag" alt="" width="32" height="18">
                    {{-- <span class="text-white"> EN</span> --}}
                </a>
            @endif
        </div><!-- /.navbar-intro -->
        <div class="navbar-menu collapse navbar-collapse navbar-backdrop " id="navbarMenu1-dark">
          <div class="navbar-nav">
            <ul class="nav nav-compact-3">
              <li class="nav-item pos-rel dropdown dropdown-mega dropdown-hover">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-list-alt mr-2 d-lg-none"></i>
                  Services
                  <i class="caret fa fa-angle-down d-none d-lg-block"></i>
                  <i class="caret fa fa-angle-left d-block d-lg-none"></i>
                </a>
                <div class="p-0 dropdown-menu dropdown-center dropdown-lg border-0 shadow radius-2 mt-1 mt-lg-3">
                  <div class="row mx-0">
                    <div class="bgc-white col-lg-6 col-12 p-2 p-lg-3 p-xl-4">
                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-blue-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-edit text-160 text-blue mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            New Post
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Praesent commodo cursus...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-green-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-comments text-160 text-green mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Comments
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Donec id elit non mi...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-orange-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-star text-160 text-orange-d2 mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>

                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Favorites
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Commodo tortor mauri...
                          </p>
                        </div>
                      </div>
                    </div><!-- .col:mega links -->

                    <div class="bgc-white col-lg-6 col-12 p-2 p-lg-3 p-xl-4">
                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-blue-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-globe text-160 text-blue mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Links
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Praesent commodo cursus...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-purple-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-copy text-160 text-purple mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Documents
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Donec id elit non mi...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-red-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-list text-160 text-danger mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Mega Menu
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Commodo tortor mauri...
                          </p>
                        </div>
                      </div>
                    </div><!-- .col:mega links -->
                  </div><!-- .row: mega -->


                  <div class="px-4 mx-0 order-first order-lg-last bgc-primary-l4 border-t-1 brc-secondary-l2">
                    <div class="py-3">
                      <button class="mx-2px btn px-25 py-2 text-100 btn-blue radius-round px-4 border-2">
                        Get Started Now
                      </button>

                      <div class="d-block d-md-inline-block mt-3 mt-md-0">
                        <span class="text-dark-tp3 ml-3">
                                or call us now to learn more
                              </span>

                        <b class="text-600">
                          111-222-333
                        </b>
                      </div>
                    </div>
                  </div><!-- .row:megamenu big buttons -->
                </div>
              </li>


              {{-- For Buy --}}
              <li class="nav-item pos-rel dropdown dropdown-mega dropdown-hover">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-list-alt mr-2 d-lg-none"></i>
                  Buy
                  <i class="caret fa fa-angle-down d-none d-lg-block"></i>
                  <i class="caret fa fa-angle-left d-block d-lg-none"></i>
                </a>
                <div class="p-0 dropdown-menu dropdown-center dropdown-lg border-0 shadow radius-2 mt-1 mt-lg-3">
                  <div class="row mx-0">
                    <div class="bgc-white col-lg-6 col-12 p-2 p-lg-3 p-xl-4">
                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-blue-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-edit text-160 text-blue mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            New Post
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Praesent commodo cursus...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-green-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-comments text-160 text-green mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Comments
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Donec id elit non mi...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-orange-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-star text-160 text-orange-d2 mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>

                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Favorites
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Commodo tortor mauri...
                          </p>
                        </div>
                      </div>
                    </div><!-- .col:mega links -->

                    <div class="bgc-white col-lg-6 col-12 p-2 p-lg-3 p-xl-4">
                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-blue-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-globe text-160 text-blue mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Links
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Praesent commodo cursus...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-purple-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-copy text-160 text-purple mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Documents
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Donec id elit non mi...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-red-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-list text-160 text-danger mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Mega Menu
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Commodo tortor mauri...
                          </p>
                        </div>
                      </div>
                    </div><!-- .col:mega links -->
                  </div><!-- .row: mega -->



                </div>
              </li>
              {{-- For Sell --}}

              <li class="nav-item pos-rel dropdown dropdown-mega dropdown-hover">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-list-alt mr-2 d-lg-none"></i>
                  Sell
                  <i class="caret fa fa-angle-down d-none d-lg-block"></i>
                  <i class="caret fa fa-angle-left d-block d-lg-none"></i>
                </a>
                <div class="p-0 dropdown-menu dropdown-center dropdown-lg border-0 shadow radius-2 mt-1 mt-lg-3">
                  <div class="row mx-0">
                    <div class="bgc-white col-lg-6 col-12 p-2 p-lg-3 p-xl-4">
                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-blue-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-edit text-160 text-blue mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            New Post
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Praesent commodo cursus...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-green-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-comments text-160 text-green mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Comments
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Donec id elit non mi...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-orange-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-star text-160 text-orange-d2 mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>

                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Favorites
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Commodo tortor mauri...
                          </p>
                        </div>
                      </div>
                    </div><!-- .col:mega links -->

                    <div class="bgc-white col-lg-6 col-12 p-2 p-lg-3 p-xl-4">
                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-blue-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-globe text-160 text-blue mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Links
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Praesent commodo cursus...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-purple-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="far fa-copy text-160 text-purple mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Documents
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Donec id elit non mi...
                          </p>
                        </div>
                      </div>

                      <div class="d-flex align-items-center">
                        <div class="pos-rel">
                          <i class="bgc-red-l3 w-4 h-4 radius-round position-r mr-2 mt-25"></i>
                          <i class="fa fa-list text-160 text-danger mr-2 mb-3 w-6 h-6 radius-round text-center pt-1 pos-rel"></i>
                        </div>
                        <div>
                          <a href="#" class="d-block py-1px px-1 radius-1 no-underline text-dark-tp3 text-600 bgc-h-primary-l2 text-95">
                            Mega Menu
                          </a>
                          <p class="text-dark-tp4 pl-1">
                            Commodo tortor mauri...
                          </p>
                        </div>
                      </div>
                    </div><!-- .col:mega links -->
                  </div><!-- .row: mega -->



                </div>
              </li>


              <li class="nav-item ml-xl-2">
                <a class="nav-link" href="#">
                    <i class="fa fa-user mr-15"></i>
                    Real Estate Agent
                </a>
              </li>
            </ul><!-- /.navbar-nav menu -->
          </div><!-- /.navbar-nav -->

        </div><!-- /.navbar-menu.navbar-collapse -->


        <button type="button" class="d-style btn btn-burger static collapsed mr-2 px-4 navbar-toggler d-flex d-lg-none" data-toggle="collapse" data-target="#navbarMenu2-dark" aria-controls="navbarMenu2-dark" aria-expanded="false" aria-label="Toggle navbar menu">
          <i class="fa fa-caret-down d-collapsed text-80"></i>
          <i class="fa fa-caret-up d-n-collapsed text-80"></i>

          <i class="fa fa-user ml-2"></i>
        </button><!-- mobile navbar toggler button -->


        <div class="navbar-menu collapse navbar-collapse navbar-backdrop" id="navbarMenu2-dark">
          <div class="navbar-nav">
            <ul class="nav nav-compact">
                {{-- Login --}}
                @guest
                    <li class="nav-item dropdown px-lg-2 d-lg-flex flex-column justify-content-center">
                        <a class="d-none d-lg-block h-auto btn btn-outline-white btn-bold radius-round border-2 dropdown-toggle px-2 px-xl-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            {{trans('global.login')}}
                        </a>

                        <a class="d-lg-none nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-caret-right bgc-white text-blue radius-round py-2 px-3 mr-2"></i>
                            Login
                        </a>
                        <div style="width: 20rem;" data-display="static" class="dropdown-menu dropdown-menu-right overflow-hidden dropdown-animated animated-2 shadow radius-1 p-2 p-lg-0 border-0 mt-lg-n1 mr-lg-n2">
                            {{-- login dialog --}}
                            <h4 class="text-120 mb-3 text-secondary-d3 px-3 mt-3 mb-2">{{trans('global.Please enter your details')}}</h4>
                            <hr class="border-dotted brc-default-l2" />
                            {{-- <form class="dropdown-clickable text-grey-d2"> --}}
                            <form class="dropdown-clickable text-grey-d2" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="dropdown-body my-25">
                                    <div class="form-group mx-3">
                                        <div class="d-flex align-items-center input-floating-label text-blue-m1 brc-blue-m2">
                                            <input placeholder="{{trans('global.Username')}}" type="text" name="email" class="form-control pr-4 shadow-none" autocomplete="off" />
                                            <i class="fa fa-user text-grey-m2 ml-n4"></i>
                                            <label class="floating-label text-grey-l1 ml-n3">{{trans('global.Username')}}</label>
                                        </div>
                                        @error('username')
                                            <span class="form-text form-error text-danger-m2" id="gender-error" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mx-3">
                                        <div class="d-flex align-items-center input-floating-label text-blue-m1 brc-blue-m2">
                                            <input placeholder="{{trans('global.Password')}}" type="password" name="password" class="form-control pr-4 shadow-none" autocomplete="off" />
                                            <i class="fa fa-key text-grey-m2 ml-n4"></i>
                                            <label class="floating-label text-grey-l1 ml-n3">{{trans('global.Password')}}</label>
                                        </div>
                                        @error('password')
                                            <span class="form-text form-error text-danger-m2" id="gender-error" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="mx-3">
                                        <label class="d-inline-block mt-2 text-secondary-d2">
                                            <input class="mr-1" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            {{trans('global.Remember me')}}
                                        </label>
                                    </div>
                                </div>
                                <div class="dropdown-footer text-center py-25 bgc-default-l4 border-t-1 brc-default-l2">
                                    <button type="submit" class="btn px-4 py-15 text-95 btn-blue">
                                        {{trans('global.login')}}
                                    </button>
                                    <a class="btn px-25 py-15 text-95 btn-outline-green btn-bgc-white" href="{{route('front.business.register')}}"> {{trans('global.register')}}</a>
                                    <div class="form-group mt-3 mx-5">
                                        <div class="d-flex mb-1 align-items-center input-floating-label text-blue-m1 brc-blue-m2">
                                            <a href="{{ url('/oauth/facebook') }}" class="btn btn-primary btn-block"><i class="fab fa-facebook"></i> Facebook</a>
                                        </div>
                                        <div class="d-flex mb-1 align-items-center input-floating-label text-blue-m1 brc-blue-m2">
                                            <a href="{{ url('/oauth/google') }}" class="btn btn-success btn-block"><i class="fab fa-google"></i> Google</a>
                                        </div>
                                        <div class="d-flex align-items-center input-floating-label text-blue-m1 brc-blue-m2">
                                            <a href="{{ url('/oauth/github') }}" class="btn btn-warning btn-block"><i class="fab fa-github"></i> Github</a>
                                        </div>
                                        {{-- <a href="{{ url('/oauth/twitter') }}" class="btn btn-twitter"><i class="fab fa-twitter"></i> Twitter</a> --}}
                                        {{-- <a href="{{ url('/oauth/linkedin') }}" class="btn btn-linkedin"><i class="fab fa-linkedin"></i> Linkedin</a> --}}
                                        {{-- <a href="{{ url('/oauth/bitbucket') }}" class="btn btn-bitbucket"><i class="fab fa-bitbucket"></i> Bitbucket</a> --}}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                @else
                    {{-- <li class="nav-item dropdown px-lg-2 d-lg-flex flex-column justify-content-center">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->first_name }}  {{ Auth::user()->last_name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li> --}}
                    <li class="nav-item dropdown order-first order-lg-last">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <img id="id-navbar-user-image" class="d-none d-lg-inline-block radius-round border-2 brc-white-tp1 mr-2 w-6" src="{{asset('public/backend/assets/image/avatar/avatar6.jpg')}}" alt="Jason's Photo">
                            <span class="d-inline-block d-lg-none d-xl-inline-block">
                            <span class="text-90" id="id-user-welcome">{{trans('global.Welcome')}},</span>
                            <span class="nav-user-name">{{Auth::user()->last_name}}</span>
                            </span>
                            <i class="caret fa fa-angle-down d-none d-xl-block"></i>
                            <i class="caret fa fa-angle-left d-block d-lg-none"></i>
                        </a>

                        <div class="dropdown-menu dropdown-caret dropdown-menu-right dropdown-animated brc-primary-m3 py-1">
                        <div class="d-none d-lg-block d-xl-none">
                            <div class="dropdown-header">
                                {{trans('global.Welcome')}}, {{Auth::user()->last_name}}
                            </div>
                            <div class="dropdown-divider"></div>
                        </div>

                        <div class="dropdown-clickable px-3 py-25 bgc-h-secondary-l3 border-b-1 brc-secondary-l2">
                            <!-- online/offline toggle -->
                            <div class="d-flex justify-content-center align-items-center tex1t-600">
                            <label for="id-user-online" class="text-grey-d1 pt-2 px-2">offline</label>
                            <input type="checkbox" class="ace-switch ace-switch-sm text-grey-l1 brc-green-d1" id="id-user-online" />
                            <label for="id-user-online" class="text-green-d1 text-600 pt-2 px-2">online</label>
                            </div>
                        </div>

                        <a class="mt-1 dropdown-item btn btn-outline-grey bgc-h-primary-l3 btn-h-light-primary btn-a-light-primary" href="{{url('home')}}">
                            <i class="fa fa-user text-primary-m1 text-105 mr-1"></i>
                            {{trans('global.my_profile')}}
                        </a>

                        <a class="dropdown-item btn btn-outline-grey bgc-h-success-l3 btn-h-light-success btn-a-light-success" href="#" data-toggle="modal" data-target="#id-ace-settings-modal">
                            <i class="fa fa-cog text-success-m1 text-105 mr-1"></i>
                            {{trans('global.Settings')}}
                        </a>

                        <div class="dropdown-divider brc-primary-l2"></div>

                        {{-- <a class="dropdown-item btn btn-outline-grey bgc-h-secondary-l3 btn-h-light-secondary btn-a-light-secondary" href="{{ route('logout') }}">
                            <i class="fa fa-power-off text-warning-d1 text-105 mr-1"></i>
                            Logout
                        </a> --}}

                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="dropdown-item btn btn-outline-grey bgc-h-secondary-l3 btn-h-light-secondary btn-a-light-secondary" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                    <i class="fa fa-power-off text-warning-d1 text-105 mr-1"></i>   {{trans('global.logout')}}
                                </a>
                        </form>

                        </div>
                    </li>
                @endguest
              {{-- End Login --}}

              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle px-lg-3" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="text-140 fa fa-search"></i>
                  <span class="d-lg-none ml-2">
                          Open Search
                      </span>
                </a>
                <div style="width: 20rem;" data-display="static" class="dropdown-menu dropdown-menu-right dropdown-animated animated-2 dropdown-caret radius-1 p-2 p-lg-0 border-0 mt-lg-n1 mr-lg-n2 shadow">
                  <form class="dropdown-clickable text-grey-d2">
                    <div class="dropdown-body my-25">
                      <div class="px-2 px-md-3">
                        <input type="text" class="form-control shadow-none m-0" placeholder="Enter Keywords to Search ..." />
                      </div>
                    </div>

                    <div class="dropdown-footer py-2 text-center pos-rel border-t-1 brc-secondary-l2">
                      <button type="button" class="btn px-4 btn-sm btn-primary" data-dismiss="dropdown">Find</button>
                      <button type="reset" class="btn btn-sm btn-outline-secondary">Clear</button>
                    </div>
                  </form>
                </div>
              </li>


              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="text-140 far fa-heart text-yellow-l3"></i>
                  <span class="d-lg-none ml-2 text-yellow-l3">
                          Bookmark
                      </span>
                </a>
              </li>


              <li class="nav-item px-3 py-3">
                <span class="text-white p-2 border-b-3 brc-white-tp5 bgc-h-white-tp9 radius-t-1">
                      <i class="fa fa-phone fa-flip-horizontal text-125 text-green-l4 mr-1"></i>

                      <span class="text-90 ml-1 ml-lg-0 d-lg-none d-xl-inline-block">
                          CALL NOW
                      </span>

                <span class="ml-1 text-600 letter-spacing-1 d-lg-none d-xl-inline-block">
                          855-92 771 244
                      </span>
                </span>
              </li>

            </ul>
          </div>
        </div><!-- .navbar-menu -->



      </div><!-- /.container -->
    </div><!-- /.navbar-inner -->
  </nav>
