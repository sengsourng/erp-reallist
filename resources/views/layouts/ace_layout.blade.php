@include('layouts.partials.admin.ace_head')

  <body>
    <div class="body-container">
        {{-- top navbar --}}
        @include('layouts.partials.admin.top_nav')

      <div class="main-container bgc-white">
        @guest
            @if (Route::has('login')) @endif
        @else
            @include('layouts.partials.admin.left_sidebar')
        @endguest
        <div role="main" class="main-content">
          {{-- <div class="page-content container-fluid container-plus"> --}}
            {{-- <div class="row mt-3">
              <div class="col-md-12"> --}}
                {{-- for content --}}
                @yield('content')
                {{-- {{ $slot }} --}}
              {{-- </div>
            </div> --}}

            @include('layouts.partials.admin.footer')
          {{-- </div> --}}
        </div>
      </div>
    </div>
    <!-- include common vendor scripts used in demo pages -->

    @include('layouts.partials.admin.script')
  </body>
  {{-- @livewireScripts --}}
</html>
