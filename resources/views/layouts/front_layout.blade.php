<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <base href="{{asset('/')}}" />
    <title>@yield('pagetitle','Reallist Home Page')</title>
    <!-- include common vendor stylesheets & fontawesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/regular.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/brands.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/@fortawesome/fontawesome-free/css/solid.css')}}">
    <!-- include vendor stylesheets used in "Landing Page 1" page. see "/views//pages/partials/landing-page-1/@vendor-stylesheets.hbs" -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/node_modules/aos/dist/aos.css')}}">
    <!-- include fonts -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/dist/css/ace-font.css')}}">
    <!-- ace.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/dist/css/ace.css')}}">
    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{asset('public/backend/favicon.png')}}" />
    <!-- "Landing Page 1" page styles, specific to this page for demo only -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/pages/landing-page-1/@page-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backend/dist/css/ace-themes.css')}}">

    <style>
        body{
            font-family: 'Hanuman', 'serif' !important;
        }

        @media (min-width: 1350px) {
            .image-intro{
                max-width: none;
                background-image:url(https://nhs-static.secure.footprint.net//GlobalResources14/NewHomeSource/images/optimized//homepage/state/search_background_1682.jpg);
                /* max-height: 600px !important; */
                background-repeat: no-repeat;
                /* background-size: 100% 100%;" */
                /* width: 350px;
                height: 650px; */
                object-fit: fill;
                /* object-fit: cover; */

                background-size:cover;
                /* box-shadow:inset 0 0 0 2000px rgba(5, 5, 5, 0.945); */
            }
        }
        @media (max-width: 1200px) {}
        @media (min-width: 1024px) {
            .image-intro{
                max-width: none;
                background-image:url(https://nhs-static.secure.footprint.net//GlobalResources14/NewHomeSource/images/optimized//homepage/state/search_background_1682.jpg);
                /* max-height: 600px !important; */
                background-repeat: no-repeat;
                /* background-size: 100% 100%;" */
                /* width: 350px;
                height: 650px; */
                object-fit: fill;
                /* object-fit: cover; */

                background-size:cover;
                box-shadow:inset 0 0 0 2000px rgba(0, 0, 0, 0.472);
            }
        }
        @media (max-width: 860px) {}

        @media screen and (max-width: 992px){
            .image-intro{
                max-width: none;
                background-image:url(https://nhs-static.secure.footprint.net//GlobalResources14/NewHomeSource/images/optimized//homepage/state/search_background_1682.jpg);
                /* max-height: 600px !important; */
                background-repeat: no-repeat;
                /* background-size: 100% 100%;" */
                /* width: 350px; */
                height: 650px;
                object-fit: fill;
                /* object-fit: cover; */
                /* opacity: 0.5; */
                box-shadow:inset 0 0 0 2000px rgba(0, 0, 0, 0.472);

            }
        }

        @media screen and (max-width: 375px){
            .image-intro{
                max-width: none;
                background-image:url(https://nhs-static.secure.footprint.net//GlobalResources14/NewHomeSource/images/optimized//homepage/state/search_background_1682.jpg);
                /* max-height: 600px !important; */
                background-repeat: no-repeat;
                /* background-size: 100% 100%;" */
                width: 375px;
                height: 650px;
                object-fit: fill;
                /* object-fit: cover; */
                /* opacity: 0.5; */
                box-shadow:inset 0 0 0 2000px rgba(0, 0, 0, 0.472);

            }
        }

        @media screen and (max-width: 570px){
            .image-intro{
                max-width: none;
                background-image:url(https://nhs-static.secure.footprint.net//GlobalResources14/NewHomeSource/images/optimized//homepage/state/search_background_1682.jpg);
                /* max-height: 600px !important; */
                background-repeat: no-repeat;
                /* background-size: 100% 100%;" */
                width: 540px;
                height: 650px;
                object-fit: fill;
                /* object-fit: cover; */
                /* opacity: 0.5; */
                box-shadow:inset 0 0 0 2000px rgba(0, 0, 0, 0.472);
            }
        }
    </style>
 @stack('styles')

</head>


  <body>
    <div class="body-container">
      <div class="pos-abs" id="scroll-down"></div>
        @include('frontend.layouts.top_nav')

      <div class="main-container bgc-white">

        <div role="main" class="main-content">
          <div class="page-content p-0">

            @yield('content')
          </div>
        </div>

      </div>

    </div>

    <!-- include common vendor scripts used in demo pages -->
    <script src="{{asset('public/backend/node_modules/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('public/backend/node_modules/popper.js/dist/umd/popper.js')}}"></script>
    <script src="{{asset('public/backend/node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>
    @include('sweetalert::alert')
    <!-- include vendor scripts used in "Landing Page 1" page. see "/views//pages/partials/landing-page-1/@vendor-scripts.hbs" -->
    <script src="{{asset('public/backend/node_modules/aos/dist/aos.js')}}"></script>
    <!-- include ace.js -->
    <script src="{{asset('public/backend/dist/js/ace.js')}}"></script>
    <!-- demo.js is only for Ace's demo and you shouldn't use it -->
    <script src="{{asset('public/backend/app/browser/demo.js')}}"></script>
    <!-- "Landing Page 1" page script to enable its demo functionality -->
    <script src="{{asset('public/backend/pages/landing-page-1/@page-script.js')}}"></script>
  </body>

</html>
