
<script src="{{ asset('public/backend') }}/node_modules/jquery/dist/jquery.js"></script>
<script src="{{ asset('public/backend') }}/node_modules/popper.js/dist/umd/popper.js"></script>
<script src="{{ asset('public/backend') }}/node_modules/bootstrap/dist/js/bootstrap.js"></script>
@include('sweetalert::alert')
<script src="{{ asset('public/backend') }}/node_modules/select2/dist/js/select2.js"></script>

<script src="{{ asset('public/backend') }}/dist/js/ace.js"></script>
<script src="{{ asset('public/backend/script_translate.js')}}"></script>

@stack('scripts')



