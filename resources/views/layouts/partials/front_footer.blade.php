
            <div class="footer h-auto">
                <div class="footer-inner py-45">
                  <div class="container container-plus" data-aos="fade">
                    <div class="row">
                      <div class="col-12 col-lg-5">
                        <h2 class="text-dark-m3">
                          <i class="fa fa-leaf text-85 mr-1 text-success-m1"></i>
                          ERP
                          <span class="text-75 text-dark-l2">Real List</span>
                        </h2>

                        <p class="text-90">
                          Praesent commodo cursus magna!
                        </p>

                        <div class="text-150 mt-2">
                          <i class="bgc-blue-m1 p-25 w-6 radius-round fab fa-twitter text-white mx-1"></i>
                          <i class="bgc-primary-d2 p-25 w-6 radius-round fab fa-facebook-square text-white mx-1"></i>
                          <i class="bgc-purple p-25 w-6 radius-round fab fa-instagram text-white mx-1"></i>
                        </div>
                      </div>

                      <div class="col-12 col-lg-7 mt-5 mt-lg-0">
                        <div class="row text-center text-lg-left">
                          <div class="col-4">
                              <h5 class="text-dark-tp3  mb-3">{{trans('global.Join us')}}</h5>
                                <div>
                                    <a href="#" class="text-dark-tp4">Be an Agent</a>
                                    <br />
                                    <a href="#" class="text-dark-tp4">Get referrals</a>
                                    <br />
                                    <a href="#" class="text-dark-tp4">See all jobs</a>
                                </div>
                            </div>

                          <div class="col-4 ">
                            <h5 class="text-dark-tp3 mb-3">{{trans('global.About us')}}</h5>
                            <div>
                              <a href="#" class="text-dark-tp4">Our Mission</a>
                              <br />
                              <a href="#" class="text-dark-tp4">Investors</a>
                              <br />
                              <a href="#" class="text-dark-tp4">Blog</a>
                            </div>
                          </div>



                          <div class="col-4">
                            <h5 class="text-dark-tp3 mb-3">{{trans('global.Contact us')}}</h5>
                            <div>
                              <a href="#" class="text-dark-tp4">Email</a>
                              <br />
                              <a href="#" class="text-dark-tp4">Phone</a>
                              <br />
                              <a href="#" class="text-dark-tp4">Address</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div><!-- /.row -->

                  </div><!-- /.container -->
                </div><!-- /.footer-inner -->
              </div>
              {{-- /.footer --}}

              <div class="text-center py-4 bgc-primary-l4 border-t-1 brc-primary-l3">
                  <span class="text-dark-m3 text-105">
                      Copyright &copy; 2021 IT-Smart
                  </span>
              </div>

              <div class="footer-tools">
                <a href="#" class="btn-scroll-up btn btn-dark px-25 py-2 text-95 border-2 radius-round mb-2 mr-2">
                  <i class="fa fa-arrow-up w-2 h-2"></i>
                </a>
              </div>
