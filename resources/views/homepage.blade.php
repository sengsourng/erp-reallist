@extends('layouts.front_layout')

@section('content')

    <!-- the page intro for dark theme -->
    <div id="page-intro-dark" class="page-intro pos-rel navbar-darkblue py-2 pt-xl-4 py-xl-5 text-white image-intro">
        <!-- some random circles -->
        <div class="d-none d-lg-block">
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.16;width:4px; height:4px; top:30%; left:85%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.2;width:6px; height:6px; top:91%; left:15%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.3;width:8px; height:8px; top:1%; left:87%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.24;width:6px; height:6px; top:0%; left:70%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.17;width:16px; height:16px; top:79%; left:20%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.35;width:6px; height:6px; top:10%; left:0%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.17;width:14px; height:14px; top:16%; left:87%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.15;width:17px; height:17px; top:82%; left:71%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.2;width:12px; height:12px; top:17%; left:92%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.19;width:12px; height:12px; top:44%; left:64%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.27;width:11px; height:11px; top:43%; left:89%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.18;width:9px; height:9px; top:55%; left:25%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.38;width:10px; height:10px; top:5%; left:71%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.11;width:9px; height:9px; top:62%; left:22%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.39;width:18px; height:18px; top:47%; left:3%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.18;width:6px; height:6px; top:12%; left:20%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.34;width:10px; height:10px; top:97%; left:95%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.33;width:7px; height:7px; top:74%; left:87%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.17;width:7px; height:7px; top:84%; left:72%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.14;width:9px; height:9px; top:7%; left:23%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.39;width:15px; height:15px; top:94%; left:91%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.28;width:14px; height:14px; top:60%; left:83%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.3;width:15px; height:15px; top:65%; left:66%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.1;width:7px; height:7px; top:3%; left:95%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.39;width:10px; height:10px; top:20%; left:53%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.14;width:13px; height:13px; top:13%; left:77%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.38;width:13px; height:13px; top:76%; left:13%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.19;width:16px; height:16px; top:9%; left:42%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.21;width:16px; height:16px; top:37%; left:69%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.11;width:4px; height:4px; top:21%; left:95%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.16;width:10px; height:10px; top:76%; left:74%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.16;width:17px; height:17px; top:9%; left:88%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.21;width:15px; height:15px; top:79%; left:131%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.37;width:14px; height:14px; top:94%; left:8%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.2;width:9px; height:9px; top:65%; left:87%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.1;width:16px; height:16px; top:4%; left:93%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.32;width:18px; height:18px; top:36%; left:81%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.35;width:7px; height:7px; top:0%; left:21%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.22;width:11px; height:11px; top:69%; left:82%;"></div>
            <div class="pos-abs bgc-white radius-round" style="opacity: 0.32;width:6px; height:6px; top:68%; left:79%;"></div>
        </div>

        <div class="row container container-plus mx-auto mt-3 mb-5">
            <div class="col-12 col-md-7 d-flex flex-column justify-content-center text-center" data-aos="fade-right" data-aos-delay="100">
            <div class="text-center mt-3">
                <div class="text-white">
                    <div class="p-25 m-0">
                        <ul class="nav nav-tabs nav-tabs-simple nav-tabs-scroll" role="tablist">
                            <li class="nav-item mr-2" >
                                <a  class="nav-link p-1 radius-0 active text-white" id="home1-tab-btn" data-toggle="tab" href="#home1" role="tab" aria-controls="home1" aria-selected="true">
                                {{-- <i class="fa fa-search text-success mr-3px"></i> --}}
                                Find a Home
                                </a>
                            </li>


                            <li class="nav-item  mr-2">
                                <a class="nav-link p-1 radius-0 text-white" id="home2-tab-btn" data-toggle="tab" href="#home2" role="tab" aria-controls="home3" aria-selected="false">
                                    {{-- <i class="fa fa-eye text-blue mr-3px"></i> --}}
                                    Sell a Home
                                </a>
                                </li>

                            <li class="nav-item">
                                <a class="nav-link p-1 radius-0 text-white" id="home3-tab-btn" data-toggle="tab" href="#home3" role="tab" aria-controls="home3" aria-selected="false">
                                {{-- <i class="fa fa-eye text-blue mr-3px"></i> --}}
                                Estimate
                                </a>
                            </li>
                            </ul>
                    </div>

                    <div class="tab-content tab-sliding px-0 text-white-d3 border-0">
                        <div class="tab-pane show px-25 active" id="home1" role="tabpanel" aria-labelledby="home1-tab-btn">
                            <h3>Find homes first. Tour homes fast.</h3>
                            <div class="input-group">

                                <input type="text" class="form-control form-control" id="form-field-mask-1" inputmode="text" placeholder="City,Address,School,Agent">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                    {{-- <i class="far fa-search-location mr-1"></i> --}}
                                    <i class="fas fa-search-location mr-1"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane show px-25 active" id="home2" role="tabpanel" aria-labelledby="home2-tab-btn">
                            <h3>1% listing fee when you buy + sell.</h3>

                            <div class="input-group">
                                <input type="text" class="form-control form-control" id="form-field-mask-1" inputmode="text" placeholder="Enter your address">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                    {{-- <i class="far fa-search-location mr-1"></i> --}}
                                    {{-- <i class="fas fa-search-location mr-1"></i> --}}
                                    Next
                                    </button>
                                </div>
                            </div>
                            </div>

                            <div class="tab-pane show px-25 active" id="home3" role="tabpanel" aria-labelledby="home3-tab-btn">
                            <div class="tab-pane show px-25 active" id="home1" role="tabpanel" aria-labelledby="home1-tab-btn">
                                <h3>See your home's Redfin Estimate</h3>
                                <div class="input-group">
                                    <input type="text" class="form-control form-control" id="form-field-mask-1" inputmode="text" placeholder="Enter your address">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                        {{-- <i class="far fa-search-location mr-1"></i> --}}
                                        <i class="fas fa-search-location mr-1"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                    </div>
            </div>
            </div><!-- /.col -->
            <div class="col-12 col-md-5 order-first order-md-last mb-4 mb-md-0" data-aos="fade-left">
            <img alt="Product Image" src="https://www.gha.org.uk/__data/assets/image/0015/2148/varieties/wide-3-4col.png" class="w-90 1mx-auto" />
            </div><!-- /.col -->
        </div><!-- /.row -->

    </div><!-- /#page-intro-dark -->

    <!-- the page intro for light theme -->
    <div id="page-intro-light" class="d-none page-intro pos-rel bgc-primary-l4 py-2 pt-xl-4 py-xl-5 overflow-hidden">
        <!-- some random shapes -->
        <div class="d-none d-lg-block">
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.25;width:17px; height:17px; top:89%; left:90%;"></div>
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.36;width:15px; height:15px; top:20%; left:61%;"></div>
        <div class="pos-abs bgc-green radius-round" style="opacity: 0.38;width:10px; height:10px; top:78%; left:93%;"></div>
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.2;width:12px; height:12px; top:13%; left:64%;"></div>
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.37;width:18px; height:18px; top:27%; left:88%;"></div>
        <div class="pos-abs bgc-green radius-round" style="opacity: 0.31;width:15px; height:15px; top:32%; left:56%;"></div>
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.34;width:16px; height:16px; top:35%; left:90%;"></div>
        <div class="pos-abs bgc-green radius-round" style="opacity: 0.36;width:17px; height:17px; top:76%; left:32%;"></div>
        <div class="pos-abs bgc-green radius-round" style="opacity: 0.3;width:14px; height:14px; top:16%; left:91%;"></div>
        <div class="pos-abs bgc-green radius-2px" style="opacity: 0.25;width:17px; height:17px; top:67%; left:96%; transform: rotate(355deg)"></div>
        <div class="pos-abs bgc-primary radius-2px" style="opacity: 0.39;width:19px; height:19px; top:63%; left:58%; transform: rotate(325deg)"></div>
        <div class="pos-abs bgc-orange radius-2px" style="opacity: 0.26;width:23px; height:23px; top:49%; left:58%; transform: rotate(280deg)"></div>
        <div class="pos-abs bgc-primary radius-2px" style="opacity: 0.27;width:12px; height:12px; top:57%; left:92%; transform: rotate(354deg)"></div>
        <div class="pos-abs brc-primary shape-triangle radius-1" style="opacity: 0.39;border-width:0 17px 27px 17px; top:14%; left:63%; transform: rotate(130deg)"></div>
        <div class="pos-abs brc-green shape-triangle radius-1" style="opacity: 0.22;border-width:0 12px 19px 12px; top:15%; left:97%; transform: rotate(346deg)"></div>
        <div class="pos-abs brc-orange shape-triangle radius-1" style="opacity: 0.35;border-width:0 19px 30px 19px; top:69%; left:82%; transform: rotate(319deg)"></div>
        <div class="pos-abs brc-green shape-triangle radius-1" style="opacity: 0.31;border-width:0 19px 30px 19px; top:6%; left:29%; transform: rotate(59deg);"></div>
        <div class="pos-abs brc-orange shape-triangle radius-1" style="opacity: 0.21;border-width:0 14px 22px 14px; top:57%; left:95%; transform: rotate(333deg)"></div>
        <div class="pos-abs brc-primary shape-triangle radius-1" style="opacity: 0.24;border-width:0 16px 25px 16px; top:46%; left:50%; transform: rotate(33deg)"></div>
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.29;width:18px; height:18px; top:11%; left:78%;"></div>
        <div class="pos-abs bgc-green radius-round" style="opacity: 0.34;width:11px; height:11px; top:10%; left:73%;"></div>
        <div class="pos-abs bgc-orange radius-round" style="opacity: 0.31;width:17px; height:17px; top:39%; left:92%;"></div>
        <div class="pos-abs bgc-orange radius-2px" style="opacity: 0.38;width:19px; height:19px; top:79%; left:14%; transform: rotate(339deg)"></div>
        <div class="pos-abs bgc-primary radius-round" style="opacity: 0.21;width:14px; height:14px; top:85%; left:14%;"></div>
        <div class="pos-abs bgc-green radius-round" style="opacity: 0.27;width:11px; height:11px; top:44%; left:15%;"></div>
        <div class="pos-abs bgc-primary radius-2px" style="opacity: 0.25;width:13px; height:13px; top:32%; left:13%; transform: rotate(331deg)"></div>
        </div>
        <div class="row container container-plus mx-auto mt-4 mb-5">
        <div class="col-12 col-md-7 d-flex flex-column justify-content-center text-center" data-aos="fade-right" data-aos-delay="100">
            <h1 class="text-dark-m2 pb-2 align-self-center">
                <span class="text-blue-d3">
                    Our tech
                </span>

                <span class="text-90">
                    makes your life easier...
                </span>
            </h1>

            <h4 class="my-3 text-dark-tp2">
            Start using this amazing app and see instant results!
            </h4>

            <h6 class="my-3 bgc-blue-d2 text-120 text-white align-self-center p-2 radius-3px">
            Or put a slideshow here ...
            </h6>

            <div class="mt-1 mb-3">
                <div class="text-uppercase text-600 text-95 text-dark-m3 mb-1">
                    Get it from
                </div>

                <a href="#" class="no-underline">
                    <img alt="Google Play Button" src="{{asset('public/backend/assets/image/landing/google-play-badge.png')}}" width="180" />
                </a>

                <a href="#" class="no-underline">
                <img alt="Apple Store Button" src="{{asset('public/backend/assets/image/landing/app-store-badge.svg')}}" width="145" />
                </a>
            </div>
            <div class="mt-0 form-group">
            <select autocomplete="off" class="theme-select col-12 col-sm-6 col-lg-5 col-xl-4 mx-auto form-control ace-select border-1 bgc-white brc-info-m1 text-dark-m3 h-auto py-1">
                <option value="" class="text-600">Change Theme/Color</option>
                <option value="" class="text-secondary-l3">_______________________</option>
                <option value="darkblue" class="text-primary-d1 text-600">Blue</option>
                <option value="teal" class="text-green-d1 text-600">Green</option>
                <option value="purple" class="text-purple-d1 text-600">Purple</option>
                <option value="" class="text-secondary-l3">_______________________</option>
                <option value="light" class="bgc-primary-l3">Light</option>
                <option value="white">White</option>
            </select>
            </div>
        </div><!-- /.col -->

        <div class="col-12 col-md-5 order-first order-md-last mb-4 mb-md-0" data-aos="fade-left">
            <img alt="Product Image" src="{{asset('public/backend/assets/image/landing/preview.png')}}" class="w-90 1mx-auto" />
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /#page-intro-light -->

    <div class="bgc-white">
        <!-- or use a different color -->
        <div class="container container-plus pos-rel mt-n5 text-center py-2">
            <div class="row mt-n4">
                <div class="col-12 col-lg-10 col-xl-8 mx-auto">
                    <div class="row my-4">
                    <!-- features -->
                    <div class="col-12 col-md-4 mb-4 mb-md-0" data-aos="fade-up" data-aos-delay="300">
                        <div class="feature-item radius-2 bgc-white shadow-1 p-4 h-100">
                        <div class="d-inline-block pos-rel text-center py-2 px-3 text-150">
                            <!-- the lines beneath icon -->
                            <div class="rotate-n45 brc-purple-m4 border-t-2 w-75 position-tl mt-35 mr-1"></div>
                            <div class="rotate-n45 brc-purple-m4 border-t-3 w-90 position-br mr-1 mb-425"></div>
                            <div class="rotate-n45 brc-purple-m4 border-t-2 w-90 position-bl mb-4 ml-35"></div>

                            <i class="fa fa-rocket fa-2x text-purple-d1 pos-rel"></i>
                        </div>

                        <h3 class="text-secondary-d3 text-160 my-3">
                            {{trans('global.Speed')}}
                        </h3>

                        <p class="text-dark-m3">
                            {{trans('global.Make your sales fast and easy')}} ..
                        </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-4  mb-md-0" data-aos="fade-up" data-aos-delay="450">
                        <div class="feature-item radius-2 bgc-white shadow-1 p-4 h-100">
                        <div class="d-inline-block pos-rel text-center p-2 text-150">
                            <!-- the lines beneath icon -->
                            <div class="brc-blue-m4 border-t-2 w-75 position-tl mt-3 ml-n2"></div>
                            <div class="brc-blue-m4 border-t-3 w-90 position-lc ml-n1 mt-n2"></div>
                            <div class="brc-blue-m4 border-t-2 w-90 position-bl mb-4 ml-n3"></div>

                            <i class="fa fa-running fa-2x text-blue-d1 pos-rel"></i>
                        </div>

                        <h3 class="text-secondary-d3 text-160 my-3">
                            {{trans('global.Flexibility')}}

                        </h3>

                        <p class="text-dark-m3">
                            {{trans('global.Increase convenience and customer needs')}}​...
                        </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-4 mb-md-0" data-aos="fade-up" data-aos-delay="600">
                        <div class="feature-item radius-2 bgc-white shadow-1 p-4 h-100">
                        <div class="d-inline-block text-center p-2 text-150 pos-rel">
                            <!-- the circles beneath icon -->
                            <div class="brc-orange-m4 border-2 w-3 h-3 radius-round position-tl mt-2 ml-n1"></div>
                            <div class="brc-orange-m4 border-2 w-2 h-2 radius-round position-tr mt-n1 ml-n1"></div>
                            <div class="brc-orange-m4 border-2 w-4 h-4 radius-round position-br mb-2"></div>

                            <i class="fa fa-key fa-2x text-orange pos-rel"></i>
                        </div>

                        <h3 class="text-secondary-d3 text-160 my-3">
                            Security
                        </h3>

                        <p class="text-dark-m3">
                            Nulla vitae elit libero, a pharetra augue mollis interdum...
                        </p>
                        </div>
                    </div>

                    </div><!-- /.row -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <div class="bgc-white">
        <!-- or use a different color -->
        <div class="container container-plus py-3 pos-rel">
            <!-- the large circle on right the green square on left -->
            <div data-aos="zoom-in" class="d-none d-lg-block  position-br bgc-purple-l3 mb-5 mr-5 opacity-1 radius-100" style="width: 180px; max-width: 80vw; height: 180px; max-height: 80vw;"></div>
            <div data-aos="zoom-in" class="d-none d-lg-block position-lc bgc-success-l3 opacity-1 ml-5 mt-5 radius-1" style="width: 100px; height: 100px;"></div>
            <div class="row pt-45 mt-1 mt-lg-5">
            <div class="col-12 col-lg-10 col-xl-8 mx-auto">
                <div class="d-flex flex-column align-items-center flex-md-row align-items-md-start">
                <div class="radius-2 mt-1 mr-md-5 pos-rel" data-aos="fade">
                    <!-- the small squares -->
                    <div class="position-tl bgc-grey-l3 mt-n45 ml-n45 radius-1" style="width: 100px; height: 100px;"></div>
                    <div class="position-br bgc-brown-l3 mb-n45 mr-n45 radius-1" style="width: 110px; height: 110px;"></div>
                    <div class="overflow-hidden radius-1 pos-rel border-1 p-2px brc-secondary-l2 bgc-white">
                    <img alt="Do More" src="https://ssl.cdn-redfin.com/v369.2.1/images/merch/generalImages/CompleteSolution_Q1_2020_557_YardSign2.jpg" width="420" />
                    </div>
                </div>
                <div class="flex-grow-1 text-dark-tp3 mt-4 mt-md-0 ml-md-2">
                    <h3 class="text-primary-d2 my-4 text-center text-md-left" data-aos="fade-up">
                    Sell for more than the home next door
                    </h3>

                    <div data-aos="fade-up">
                    <p>
                        Local Redfin Agents price your home right and make it shine online. Get started with a free consultation.
                    </p>
                    </div>
                    <div class="px-0 col-12 col-sm-8 col-lg-10 mx-auto radius-round bgc-white input-group">
                    <input class="form-control typeahead scrollable tt-input" type="text" placeholder="Enter your street address" id="id-typeahead" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;">
                    <input type="button" class="btn btn-pink px-4" value="{{trans('global.Join')}}">
                    </div>
                </div>
                </div>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <div class="bgc-white mt-4 mt-lg-5">
        <div class="container container-plus py-2 py-lg-4">
        <div class="row">
            <div class="col-12 col-lg-10 col-xl-8 mx-auto">
            <div class="d-flex flex-column align-items-center flex-md-row align-items-md-start">

                <div class="flex-grow-1 text-dark-tp3 mt-4 mt-md-0">
                <h3 data-aos="fade-up" class="text-primary-d2 my-4 text-center text-md-left">
                    Get real-time market updates
                </h3>

                <div data-aos="fade-up">
                    <p>
                    We’re bringing you the latest on how COVID-19 is impacting the real estate market.
                    </p>
                </div>

                <p class="mt-md-5" data-aos="fade-right">
                    <a href="#" class="md-3 mt-md-4 btn btn-outline-default btn-bold btn-bgc-white">
                    See Housing news
                    </a>
                </p>
                </div>


                <div class="radius-2 mt-1 ml-md-5 pos-rel order-first order-md-last" data-aos="fade-left">
                <!-- the small circles -->
                <div class="position-tr bgc-primary-l3 mt-n45 mr-n45 radius-100" style="width: 80px; max-width: 80vw; height: 80px; max-height: 80vw;"></div>
                <div class="position-bl bgc-orange-l3 mb-n5 ml-n45 radius-100" style="width: 100px; max-width: 80vw; height: 100px; max-height: 80vw;"></div>

                <div class="overflow-hidden radius-1 pos-rel border-1 p-2px brc-secondary-l2 bgc-white">
                    <img alt="Be Flexible" src="https://ssl.cdn-redfin.com/v369.2.1/images/merch/generalImages/Market_Updates_Desktop_1.jpg" width="420" />
                </div>
                </div>
            </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <div class="bgc-white">
        <!-- or use a different color -->
        <div class="container container-plus py-3 pos-rel">
            <!-- the large circle on right the green square on left -->
            <div data-aos="zoom-in" class="d-none d-lg-block  position-br bgc-purple-l3 mb-5 mr-5 opacity-1 radius-100" style="width: 180px; max-width: 80vw; height: 180px; max-height: 80vw;"></div>
            <div data-aos="zoom-in" class="d-none d-lg-block position-lc bgc-success-l3 opacity-1 ml-5 mt-5 radius-1" style="width: 100px; height: 100px;"></div>
            <div class="row pt-45 mt-1 mt-lg-5">
            <div class="col-12 col-lg-10 col-xl-8 mx-auto">
                <div class="d-flex flex-column align-items-center flex-md-row align-items-md-start">
                <div class="radius-2 mt-1 mr-md-5 pos-rel" data-aos="fade">
                    <!-- the small squares -->
                    <div class="position-tl bgc-grey-l3 mt-n45 ml-n45 radius-1" style="width: 100px; height: 100px;"></div>
                    <div class="position-br bgc-brown-l3 mb-n45 mr-n45 radius-1" style="width: 110px; height: 110px;"></div>
                    <div class="overflow-hidden radius-1 pos-rel border-1 p-2px brc-secondary-l2 bgc-white">
                    <img alt="Do More" src="https://ssl.cdn-redfin.com/v369.2.1/images/merch/generalImages/sidebyside_ouragents_Q3-Photoshoot-SHOT01-A-0351-2x.jpg" width="420" />
                    </div>
                </div>
                <div class="flex-grow-1 text-dark-tp3 mt-4 mt-md-0 ml-md-2">
                    <h3 class="text-primary-d2 my-4 text-center text-md-left" data-aos="fade-up">
                        Hot market. Low listing fee. More money for you.
                    </h3>
                    <div data-aos="fade-up">
                    <p>
                        Save thousands when you list with a local Redfin Agent. We’ll make your home shine online to attract buyers and sell for more.
                    </p>
                    </div>
                    <p class="mt-md-5" data-aos="fade-left">
                    <a href="#" class="mt-3 mt-md-4 btn btn-outline-default btn-bold btn-bgc-white">
                        Learn More..
                    </a>
                    </p>
                </div>
                </div>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    {{-- the 3 tabs ... So why is everybody choosing us? --}}
    @include('frontend.home.whychoosing')

    {{-- Other features --}}
    @include('frontend.home.feature')

    {{-- Testimonials --}}
    @include('frontend.home.testimonials')

    {{-- Our Packages --}}
    @include('frontend.home.packages')

    <div class="navbar-darkblue pos-rel">
        <div class="container container-plus py-5 px-lg-5">
        <div class="row">
            <div class="col-lg-6 col-xl-5 mx-auto" data-aos="fade-up">
            <h3 class="text-white text-center text-180 mb-3">
                {{trans('global.Sign up for our newsletter')}}
            </h3>

            <p class="text-white-tp1 mb-4">
                {{trans('global.To get more information update, please fill up your email and click Join')}}
            </p>

            <div>
                <div class="px-0 col-12 col-sm-8 col-lg-10 mx-auto radius-round bgc-white input-group">
                <input placeholder="Your email address" type="text" class="form-control form-control-lg radius-round shadow-none border-0 text-dark" />
                <a href="#" class="btn radius-round btn-pink text-600 text-110 px-4 m-3px">
                    {{trans('global.Join')}}
                </a>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    @include('layouts.partials.front_footer')

@endsection
