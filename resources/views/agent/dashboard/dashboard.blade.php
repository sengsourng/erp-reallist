@extends('frontend.layouts.base')



@section('content')
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Dashboard</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-12 col-md-12">
                @include('agent.includes.profile')


                <div class="card my-select">
                    <div class="card-header">
                        <h3 class="card-title">Search Ads</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <input type="text" class="form-control" id="text" placeholder="What are you looking for?">
                        </div>
                        <div class="form-group">
                            <select name="country" id="select-countries" class="form-control custom-select select2-show-search">
                                <option value="1" selected="">All Categories</option>
                                <option value="2">RealEstate</option>
                                <option value="3">Apartments</option>
                                <option value="4">3BHK Flat</option>
                                <option value="5">Homes</option>
                                <option value="6">Luxury Rooms</option>
                                <option value="7">Deluxe Houses</option>
                                <option value="8">Duplex House</option>
                                <option value="9">Luxury Rooms</option>
                                <option value="10">Pets &amp; Flats</option>
                                <option value="11">Apartments</option>
                                <option value="12">Duplex Houses</option>
                                <option value="13">3BHK Flatss</option>
                                <option value="14">2BHK Flats</option>
                                <option value="15">Modren Houses</option>
                            </select>
                        </div>
                        <div class="">
                            <a href="#" class="btn  btn-primary">Search</a>
                        </div>
                    </div>
                </div>
                <div class="card mb-xl-0">
                    <div class="card-header">
                        <h3 class="card-title">Safety Tips For Buyers</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li class="">
                                <i class="fa fa-check text-success" aria-hidden="true"></i> Meet Seller at public Place
                            </li>
                            <li class="">
                                <i class="fa fa-check text-success" aria-hidden="true"></i> Check item before you buy
                            </li>
                            <li class="">
                                <i class="fa fa-check text-success" aria-hidden="true"></i> Pay only after collecting item
                            </li>
                            <li class="ml-5 mb-0">
                                <a href="tips.html"> View more..</a>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Dashboard</h3>
                    </div>
                    <div class="card-body">
                        <div class="side-app">

                            <div class="row row-cards">
                                <div class="col-sm-12 col-lg-6 col-xl-4">
                                    <div class="card">
                                        <div class="card-body iconfont text-center">
                                            <h5 class="mb-2">Properties For Sale</h5>
                                            <h1 class="mb-2 text-primary font-weight-bold">459</h1>
                                            <p><span class="text-green"><i class="fa fa-arrow-up text-green"> </i>23%</span> in Last Month</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-6 col-xl-4">
                                    <div class="card">
                                        <div class="card-body iconfont text-center">
                                            <h5 class="mb-2">Properties For Rent</h5>
                                            <h1 class="mb-2 text-secondary font-weight-bold">875</h1>
                                            <p><span class="text-green"><i class="fa fa-arrow-up text-green"></i> 3.25%</span> in Last Month </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-6 col-xl-4">

                                    <div class="card">
                                        <div class="card-body iconfont text-center">
                                            <h5 class="mb-2">Properties For Buy</h5>
                                            <h1 class="mb-2 text-warning font-weight-bold">525</h1>
                                            <p><span class="text-red"><i class="fa fa-arrow-down text-red"></i> 0.67%</span> in Last Month</p>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-xl-4 col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="team-section text-center">
                                                <div class="team-img">
                                                    <img src="../assets/images/faces/female/1.jpg" class="img-thumbnail rounded-circle alt=" alt="">
                                                </div>
                                                <h4 class="font-weight-bold dark-grey-text mt-4">Harry	Walker</h4>
                                                <h6 class="mb-3 text-muted ">RealEstate Agent</h6>
                                                <p class="font-weight-normal dark-grey-text">
                                                <i class="fa fa-quote-left pr-2"></i>Ut enim ad minima veniam, quis nostrum exercitationem ullam quis nostrum  corporis suscipit laboriosam, nisi ut aliquid commodi.</p>
                                                <div class="text-warning">
                                                    <i class="fa fa-star"> </i>
                                                    <i class="fa fa-star"> </i>
                                                    <i class="fa fa-star"> </i>
                                                    <i class="fa fa-star"> </i>
                                                    <i class="fa fa-star"> </i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Send Messages</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="name1" placeholder="Your Name">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="email" placeholder="Email Address">
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" name="example-textarea-input" rows="3" placeholder="Message"></textarea>
                                            </div>
                                            <a href="#" class="btn btn-primary">Submit</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class=" card">
                                        <div class="card-header">
                                            <h3 class="card-title">Premium Homes</h3>
                                        </div>

                                        <div class="card-body">
                                            <ul class="list-unstyled widget-spec  mb-0">
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>Your Profile is Highlighted and It Be Top Position
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>Multiple Premium Home Applications
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>You Get Every Properties Notification
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>24/7 Customer Support Help
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>Government Id Proof Verified
                                                </li>
                                                <li class="mb-0">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>Daily 4 RealEstate Notifications
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>24/7 Customer Support Help
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i>Government Id Proof Verified
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!--/User Dashboard-->

<!-- Newsletter-->
<section class="sptb bg-white border-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-xl-6 col-md-12">
                <div class="sub-newsletter">
                    <h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-5 col-xl-6 col-md-12">
                <div class="input-group sub-input mt-1">
                    <input type="text" class="form-control input-lg " placeholder="Enter your Email">
                    <div class="input-group-append ">
                        <button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
                            Subscribe
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Newsletter-->

@endsection
