@extends('frontend.layouts.base')
@section('content')
<!--Breadcrumb-->
<section>
    <div class="bannerimg cover-image bg-background3" data-image-src="../assets/images/banners/banner2.jpg">
        <div class="header-text mb-0">
            <div class="container">
                <div class="text-center text-white">
                    <h1 class="">My Favorite Ads</h1>
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Dashboard</a></li>
                        <li class="breadcrumb-item active text-white" aria-current="page">My Favorite Ads</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Breadcrumb-->

<!--User Dashboard-->
<section class="sptb">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-12 col-md-12">
                {{-- <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">My Dashboard</h3>
                    </div>
                    <div class="card-body text-center item-user border-bottom">
                        <div class="profile-pic">
                            <div class="profile-pic-img">
                                <span class="bg-success dots" data-toggle="tooltip" data-placement="top" title="online"></span>
                                <img src="../assets/images/faces/male/25.jpg" class="brround" alt="user">
                            </div>
                            <a href="userprofile.html" class="text-dark"><h4 class="mt-3 mb-0 font-weight-semibold">Robert McLean</h4></a>
                        </div>
                    </div>
                    <div class="item1-links  mb-0">
                        <a href="mydash.html" class="d-flex border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-user"></i></span> Edit Profile
                        </a>
                        <a href="myads.html" class="d-flex  border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-diamond"></i></span> My Ads
                        </a>
                        <a href="myfavorite.html" class="active  d-flex border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-heart"></i></span> My Favorite
                        </a>
                        <a href="manged.html" class="d-flex  border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-folder-alt"></i></span> Managed Ads
                        </a>
                        <a href="payments.html" class=" d-flex  border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-credit-card"></i></span> Payments
                        </a>
                        <a href="orders.html" class="d-flex  border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-basket"></i></span> Orders
                        </a>
                        <a href="tips.html" class="d-flex border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-game-controller"></i></span> Safety Tips
                        </a>
                        <a href="settings.html" class="d-flex border-bottom">
                            <span class="icon1 mr-3"><i class="icon icon-settings"></i></span> Settings
                        </a>
                        <a href="#" class="d-flex">
                            <span class="icon1 mr-3"><i class="icon icon-power"></i></span> Logout
                        </a>
                    </div>
                </div> --}}

                @include('agent.includes.profile')


                <div class="card my-select">
                    <div class="card-header">
                        <h3 class="card-title">Search Ads</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <input type="text" class="form-control" id="text" placeholder="What are you looking for?">
                        </div>
                        <div class="form-group">
                            <select name="country" id="select-countries" class="form-control custom-select select2-show-search">
                                <option value="1" selected="">All Categories</option>
                                <option value="2">RealEstate</option>
                                <option value="3">Apartments</option>
                                <option value="4">3BHK Flat</option>
                                <option value="5">Homes</option>
                                <option value="6">Luxury Rooms</option>
                                <option value="7">Deluxe Houses</option>
                                <option value="8">Duplex House</option>
                                <option value="9">Luxury Rooms</option>
                                <option value="10">Pets &amp; Flats</option>
                                <option value="11">Apartments</option>
                                <option value="12">Duplex Houses</option>
                                <option value="13">3BHK Flatss</option>
                                <option value="14">2BHK Flats</option>
                                <option value="15">Modren Houses</option>
                            </select>
                        </div>
                        <div class="">
                            <a href="#" class="btn  btn-primary">Search</a>
                        </div>
                    </div>
                </div>
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">Safety Tips For Buyers</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled widget-spec  mb-0">
                            <li class="">
                                <i class="fa fa-check text-success" aria-hidden="true"></i> Meet Seller at public Place
                            </li>
                            <li class="">
                                <i class="fa fa-check text-success" aria-hidden="true"></i> Check item before you buy
                            </li>
                            <li class="">
                                <i class="fa fa-check text-success" aria-hidden="true"></i> Pay only after collecting item
                            </li>
                            <li class="ml-5 mb-0">
                                <a href="tips.html"> View more..</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-12 col-md-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h3 class="card-title">My Favorite Ads</h3>
                    </div>
                    <div class="card-body">
                        <div class="my-favadd table-responsive border-top userprof-tab">
                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Item</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Ad Status</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/f1.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="text-dark"><h4 class="font-weight-semibold">Modren Kitchen</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-25-2019 , 16:54 pm</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Sale</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Apartments</td>
                                        <td class="font-weight-semibold fs-16">$89</td>
                                        <td>
                                            <a href="#" class="badge badge-primary">Active</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/j1.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#"><h4 class="font-weight-semibold">Villa</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-22-2019 , 9:18 am</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Sale</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Homes</td>
                                        <td class="font-weight-semibold fs-16">$14,000</td>
                                        <td>
                                            <a href="#" class="badge badge-danger">Closed</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/h2.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="text-dark"><h4 class="font-weight-semibold">2BHK Rooms</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-15-2019 , 12:45 pm</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Buy </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>RealEstate</td>
                                        <td class="font-weight-semibold fs-16">$22,765</td>
                                        <td>
                                            <a href="#" class="badge badge-success">Sold</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/v1.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="tetx-dark"><h4 class="font-weight-semibold">Luxury Rooms</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-03-2019 , 12:50</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Sale</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Deluxe Houses</td>
                                        <td class="font-weight-semibold fs-16">$35,978</td>
                                        <td>
                                            <a href="#" class="badge badge-secondary">InActive</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/f1.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="text-dark"><h4 class="font-weight-semibold">Garden House</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-25-2019 , 16:54 pm</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Rent</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Apartments</td>
                                        <td class="font-weight-semibold fs-16">$89</td>
                                        <td>
                                            <a href="#" class="badge badge-primary">Active</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/j1.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="text-dark"><h4 class="font-weight-semibold">Villa</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-22-2019 , 9:18 am</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Sale</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Homes</td>
                                        <td class="font-weight-semibold fs-16">$14,000</td>
                                        <td>
                                            <a href="#" class="badge badge-danger">Closed</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/h2.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="text-dark"><h4 class="font-weight-semibold">Garden House</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-15-2019 , 12:45 pm</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Sale</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>RealEstate</td>
                                        <td class="font-weight-semibold fs-16">$22,765</td>
                                        <td>
                                            <a href="#" class="badge badge-danger">Expired</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="checkbox" value="checkbox">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <div class="media mt-0 mb-0">
                                                <div class="card-aside-img">
                                                    <a href="#"></a>
                                                    <img src="../assets/images/products/v1.png" alt="img">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-item-desc ml-4 p-0 mt-2">
                                                        <a href="#" class="text-dark"><h4 class="font-weight-semibold">Modren House</h4></a>
                                                        <a href="#"><i class="fa fa-clock-o mr-1"></i> Nov-03-2019 , 12:50</a><br>
                                                        <a href="#"><i class="fa fa-tag mr-1"></i> Rent</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Deluxe Houses</td>
                                        <td class="font-weight-semibold fs-16">$35,978</td>
                                        <td>
                                            <a href="#" class="badge badge-secondary">InActive</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-info btn-sm text-white" data-toggle="tooltip" data-original-title="Delete from Wishlist"><i class="fa fa-trash"></i></a>
                                            <a href="#" class="btn btn-primary btn-sm text-white" data-toggle="tooltip" data-original-title="Buy Now"><i class="fa fa-shopping-cart"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/User Dashboard-->

<!-- Newsletter-->
<section class="sptb bg-white border-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-xl-6 col-md-12">
                <div class="sub-newsletter">
                    <h3 class="mb-2"><i class="fa fa-paper-plane-o mr-2"></i> Subscribe To Our Newsletter</h3>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-5 col-xl-6 col-md-12">
                <div class="input-group sub-input mt-1">
                    <input type="text" class="form-control input-lg " placeholder="Enter your Email">
                    <div class="input-group-append ">
                        <button type="button" class="btn btn-primary btn-lg br-tr-3  br-br-3">
                            Subscribe
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Newsletter-->
@endsection
