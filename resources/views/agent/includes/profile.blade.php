<div class="card">
    <div class="card-header">
        <h3 class="card-title">My Dashboard</h3>
    </div>
    <div class="card-body text-center item-user border-bottom">
        <div class="profile-pic">
            <div class="profile-pic-img">
                <span class="bg-success dots" data-toggle="tooltip" data-placement="top" title="online"></span>
                <img src="{{asset('public/images/faces/default.jpg')}}" class="brround" alt="user">
            </div>
            <a href="userprofile.html" class="text-dark"><h4 class="mt-3 mb-0 font-weight-semibold">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h4></a>
        </div>
    </div>
    <div class="item1-links  mb-0">
        <a href="{{route('agent.dashboard.index')}}" class="active d-flex border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-user"></i></span> Edit Profile
        </a>
        <a href="{{route('agent.myads')}}" class=" d-flex  border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-diamond"></i></span> My Ads
        </a>
        <a href="{{route('agent.myfavorite')}}" class=" d-flex border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-heart"></i></span> My Favorite
        </a>
        <a href="{{route('agent.myfavorite')}}" class="d-flex  border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-folder-alt"></i></span> Managed Ads
        </a>
        <a href="{{route('agent.myfavorite')}}" class=" d-flex  border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-credit-card"></i></span> Payments
        </a>
        <a href="{{route('agent.myfavorite')}}" class="d-flex  border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-basket"></i></span> Orders
        </a>
        <a href="{{route('agent.myfavorite')}}" class="d-flex border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-game-controller"></i></span> Safety Tips
        </a>
        <a href="{{route('agent.myfavorite')}}" class="d-flex border-bottom">
            <span class="icon1 mr-3"><i class="icon icon-settings"></i></span> Settings
        </a>


        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="d-flex" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            this.closest('form').submit();">
                    <span class="icon1 mr-3"><i class="icon icon-power"></i></span>  {{trans('global.logout')}}
                </a>
        </form>
    </div>
</div>
