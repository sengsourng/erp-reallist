
<?php

return [

    'dashboard' => 'Dashboard',
    'user.management' => 'User Management',
    'role' => 'Manage Roles',
    'permission' => 'Manage Permissions',
    'user' => 'Manage Users',

];
