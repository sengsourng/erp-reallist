<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create admin user
        $user = User::create([
        	'first_name' => 'Admin',
            'last_name' => 'Super',
            'username' => 'admin',
        	'email' => 'admin@admin.com',
            'phone' => '078343143',
            'usertype' => 'admin',
        	'password' => bcrypt('123456')
        ]);
        $admin = Role::create(['name'=>'Admin','slug'=>'admin']);
        $user->assignRole([$admin->id]);
        //create owner role
        $role = Role::create(['name' => 'Owner','slug'=>'owner']);

        //create agent user
        $user = User::create([
        	'first_name' => 'Agent',
            'last_name' => 'User',
            'username' => 'agent',
        	'email' => 'agent@agent.com',
            'phone' => '092771244',
            'usertype' => 'agent',
        	'password' => bcrypt('123456')
        ]);

        $role = Role::create(['name' => 'Agent','slug'=>'agent']);
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);

        //create staff and user role
        $role = Role::create(['name' => 'Staff','slug'=>'staff']);
        $role = Role::create(['name' => 'User','slug'=>'user']);
    }
}
