<?php

return [
    'app_version' => 1,
    'admin_route' => env('ADMIN_ROUTE','admin'),
    'owner_route' => env('OWNER_ROUTE','owner'),
    'agent_route' => env('AGENT_ROUTE','agent'),
    'staff_route' => env('STAFF_ROUTE','staff'),
    'user_route' => env('USER_ROUTE','user')
];
